<style type="text/css">
  .hide{display: none}
</style>
<?php $this->load->helper('Audit'); ?>

<div class="container" style=" margin-bottom: 80px;">
  <div class="row" style="padding-top:90px;">

      <div class="col-md-12 ml-md-auto mb-5">
        <form action="<?php echo base_url("/audit"); ?>" method="get">
            <div class="row">

              <div class="col-md-auto col-md-2 px-2">
                  <select name="kategori" id="kategori" class="kategori form-control custom-select">
                  <option selected value="">- Pilih Kategori -</option>
                    <?php 
                      if(!empty($result_menu)){
                        foreach ($result_menu->results->travel_categories as $value) {
                              if(!empty($value->sub_travel_category)){
                                $id_kategori = $value->id;
                                $nama_kategori = $value->category_name;
                            ?>

                                <option 
                                  <?php 
                                  if(!empty($history_kategori)){
                                    if($id_kategori == $history_kategori){
                                      echo " selected ";
                                    }
                                  }
                                  ?>
                                  value="<?php echo $id_kategori; ?>">
                                    <?php 
                                      echo $nama_kategori;
                                    ?>
                                  </option>

                  <?php
                        }
                      }
                    }

                    ?>
                </select>
                </div>


                <div class="col-1 col-md-2  px-1">
                  <select name="subkategori" id="subkategori" class="subkategori subkategori_data form-control custom-select">
                  <option value="">- Pilih Sub Kategori -</option>
                </select>
                </div>

                <div class="col-auto col-md-1  px-2">
                  <select name="pencarian" id="pencarian" class="form-control custom-select">
                  <!-- <option selected value="">Pencarian</option> -->
                  <option value="1">Nasional</option>
                </select>
                </div>


                <div class="col-auto col-md-2  px-2">
                  <select name="provinsi" id="provinsi" class="form-control custom-select">
                  <option selected value="">- Pilih Provinsi -</option>
                    <?php 
                    if(!empty($result_provinsi)){
                      foreach ($result_provinsi->results->provinces as $value) {
                        $id       = $value->province_id;
                        $nama_provinsi  = $value->province_name;
                        $desc       = $value->description;

                  ?>

                        <option 
                        <?php 
                        if(!empty($history_provinsi)){
                          if($id == $history_provinsi){
                            echo " selected ";
                          }
                        }
                        ?>
                        value="<?php echo $id; ?>">
                          <?php 
                            echo $nama_provinsi;
                          ?>
                        </option>

                  <?php
                      }
                    } else{


                    }

                    ?>
                </select>
                </div>

                <!-- <div class="col-2 px-2">
                  <select name="kotakabupaten" id="kotakabupaten" class="kotakabupaten custom-select">
                  <option selected value="">Kota / Kabupaten</option>
                </select>
                </div> -->

                <div class="col-2 col-md-2 px-2">
                  <select name="kotakabupaten" id="kotakabupaten_travels" class="kotakabupaten form-control custom-select">
                  <option selected value="">- Pilih Kota / Kabupaten -</option>
                </select>
                </div>

                <!-- <div class="col-2 px-2">
                  <select name="kecamatan" class="kecamatan custom-select">
                  <option selected value="">Kecamatan</option>
                </select>
                </div> -->

                <div class="col-2 col-md-2 px-2">
                  <select name="traveling_id" class="travel form-control custom-select">
                  <option selected value="">- Pilih Objek Wisata -</option>
                </select>
                </div>


                <div class="col-1 col-md-1 px-2">
                  <select name="point" id="point" class="form-control custom-select">
                  <!-- <option selected value="">Pilih Point</option> -->
                    <?php 
                    if(!empty($result_point)){
                      foreach ($result_point->results->point_types as $value) {
                        $id           = $value->id;
                        $nama_point   = $value->point_type_name;
                        $desc         = $value->description;

                        if($nama_point != "Data"){


                  ?>

                          <option 
                          <?php 
                          if(!empty($history_point)){
                            if($id == $history_point){
                              echo " selected ";
                            }
                          }
                          ?>
                          value="<?php echo $id; ?>">
                            <?php 
                              echo $nama_point;
                            ?>
                          </option>

                  <?php
                        }
                      }
                    } else{


                    }

                    ?>
                </select>
                </div>


                </div>


              <div class="float-right my-3 ">
                <input type="submit" name="submit" value="submit" class="btn btn-primary py-2" style="width:100%;" value="cari">
                
              </div>
        </form>
                

            

      </div>
  </div>

  <div class="row">
    <h3>Hasil Audit <span class="point_name"></span></h3>
     <div class="table table-bordered table-striped table-responsive" style="height:420px;">        
                <table class="table">
                  <thead>
                    <tr>
                      <th width="3%">No</th>
                      <th>Pertanyaan</th>
                      <!-- <th class="header_evaluasi">Evaluasi</th>     -->                            
                      <?php $no_header = 0; 
                          //var_dump($result_data_objects->status);
                          //exit();
                      ?>
                      <?php if($result_data_objects->status != "ERROR"){ ?> 
                        <?php if(!!empty($result_data_objects->results->objects[0]->auth)){ ?>                          
                          <th style="background-color: rgba(0, 0, 0, 0.05);" class="type_name">
                            <?php echo $result_data_objects->results->objects[0]->type_name ?>
                          </th>
                          <?php foreach ($result_data_objects->results->objects as $object) { ?>    
                            <th style="white-space:nowrap;" >
                              <?php $no_header++; echo $object->name ?>                                
                            </th>                  
                          <?php }?>                          
                        <?php }?>  
                      <?php }?>   

                    </tr>
                  </thead>

                   <tbody id="load_more_data"> 
                    <tr>
                      <th style="text-align: right" class="colspand_jumlah">
                        Jumlah
                        <span id="offset_col" class="hide"><?php echo $no_header ?></span>
                      </th>
                      <th id="sum_all"></th>                      
                      <?php for ($i=1; $i <= (int)$no_header; $i++) {                         
                        echo"<th class='sum_split_".$i."'></th>";
                      } ?>                      
                    </tr>
                    <?php if($result_data_objects != ""){ ?>                      
                      <?php if(!!empty($result_data_objects->results->objects[0]->auth)){ ?>
                        <?php if($result_data_questions->results){ ?>              
                          <?php $nob=1;$no = 1;foreach ($result_data_questions->results->questions as $question) { 
                            if(!empty($_GET["point"])){
                            ?>
                            <?php if($_GET['point'] == '2' || $_GET['point'] == '3'){ ?>
                              <tr>
                                <td><?php echo $nob++ ?>
                                  <span class="offset hide">0</span>
                                </td>
                                <th style="white-space:nowrap;" colspan="100"><?php echo $question->question ?></th>
                              </tr>                          
                              <?php foreach ($question->points_list as $point) { ?>
                                <?php $arr_result = array(); ?>   
                                <tr>
                                  <td></td><?php $no++ ?>
                                  <td style="white-space:nowrap;" class="body_evaluasi"><?php echo $point->evaluation ?></td>
                                  <td class="collect_data_<?php echo $no ?>" style="background-color: rgba(0, 0, 0, 0.05);"></td>
                                    <?php $nos= 0;foreach ($result_data_objects->results->objects as $object) { ?>
                                      <?php $nos++ ?>                                      
                                      <td class="row_<?php echo $no ?>_<?php echo $nos ?>">
                                        <?php $result = find_audit_results($question->id, $object->id, $_GET['point'], $point->point_id, $object->type) ?>
                                        <?php echo $result; array_push($arr_result, $result); ?>        
                                      </td>                  
                                    <?php }?>                                            
                                    <span class="data_sum_<?php echo $no ?> hide"><?php echo array_sum($arr_result) ?></span>
                                </tr>                  
                              <?php }?> 
                            <?php }else{ ?>      
                              <?php $arr_result = array(); ?>                                                        
                              <tr>
                                <td><?php echo $no++ ?>
                                  <span class="offset hide">0</span>
                                </td>
                                <td style="white-space:nowrap;"><?php echo $question->question ?></td>
                                <td style="white-space:nowrap;" class="body_evaluasi"><?php echo $question->evaluation ?></td>
                                <td class="collect_data_<?php echo $no ?>" style="background-color: rgba(0, 0, 0, 0.05);"></td>
                                  <?php $nos= 0;foreach ($result_data_objects->results->objects as $object) { ?>
                                    <?php $nos++ ?>                                      
                                    <td class="row_<?php echo $no ?>_<?php echo $nos ?>">                        
                                      <?php $result = find_audit_results($question->id, $object->id, $_GET['point'], $question->point_id, $object->type) ?>
                                      <?php echo $result; array_push($arr_result, $result); ?>        
                                    </td>                  
                                  <?php }?>                                            
                                  <span class="data_sum_<?php echo $no ?> hide"><?php echo array_sum($arr_result) ?></span>
                              </tr>                            
                            <?php } ?>
                          <?php }
                          }
                          ?>                            
                          <span class="offset_row hide"><?php echo $no ?></span>                        
                        <?php }else{?>                        
                          <tr>
                            <th colspan="100"><center>Data tidak ditemukan</center></th>
                          </tr>
                        <?php }?>   
                      <?php }else{?>
                        <?php
                          echo "
                                <tr>
                                <th colspan='4' scope='row'>
                                  <div class='alert alert-info' role='alert'>
                                    <a href='".base_url()."sessions/index'>".$result_data_objects->results->objects[0]->message."</a>
                              </div>
                              </th>
                            </tr>";
                        ?>
                      <?php }?>
                    <?php }else{?>           
                      <tr>
                        <th colspan="3"><center><?php echo $result_data_objects->messages ?></center></th>
                      </tr>
                    <?php }?>                    
                   </tbody>
                </table>   
                <input type="hidden" name="kategori_ajax" value="<?php echo $this->input->get('kategori') ?>" id="kategori_ajax">
                <input type="hidden" name="subkategori_ajax" value="<?php echo $this->input->get('subkategori') ?>" id="subkategori_ajax">
                <input type="hidden" name="pencarian_ajax" value="<?php echo $this->input->get('pencarian') ?>" id="pencarian_ajax">
                <input type="hidden" name="provinsi_ajax" value="<?php echo $this->input->get('provinsi') ?>" id="provinsi_ajax">
                <input type="hidden" name="kotakabupaten_ajax" value="<?php echo $this->input->get('kotakabupaten') ?>" id="kotakabupaten_ajax">
                <input type="hidden" name="kecamatan_ajax" value="<?php echo $this->input->get('kecamatan') ?>" id="kecamatan_ajax">
                <input type="hidden" name="traveling_ajax" value="<?php echo $this->input->get('traveling_id') ?>" id="traveling_ajax">
                <input type="hidden" name="point_ajax" value="<?php echo $this->input->get('point') ?>" id="point_ajax">
    </div>
    <!-- <a href="javascript:void(0)" class="btn btn-default" id="load_more">Load more</a> -->
    <div class="image_loader hide"><img src="https://loading.io/spinners/color-bar/lg.colorful-progress-loader.gif" style="width: 100px; height: 70px"></div>
  </div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
  $("#load_more").click(function(){
    $.ajax({
      type: "GET",
      url: "audit",
      dataType: "script",
      data:{
        "offset" : ($(".offset").html() == "" ? 10 : parseInt($(".offset").html()) + 10),
        "kategori" : $("#kategori_ajax").val(),
        "subkategori" : $("#subkategori_ajax").val(),
        "pencarian" : $("#pencarian_ajax").val(),
        "provinsi" : $("#provinsi_ajax").val(),
        "kotakabupaten" : $("#kotakabupaten_ajax").val(),
        "kecamatan" : $("#kecamatan_ajax").val(),
        "traveling_id" : $("#traveling_ajax").val(),
        "point" : $("#point_ajax").val(),
        "request" : "script"
      },
      beforeSend:function(){
          $(".image_loader").removeClass("hide");
      },
      success:function(html){          
          $(".image_loader").addClass("hide");
          $result = parseInt($(".offset").html()) + 10;
          parseInt($(".offset").html($result));
          $('#load_more_data').append(html);
      }
    });
  })
  $(document).ready(function(){        
    for (var i = 2; i <= parseInt($(".offset_row").html()); i++) {
      $(".collect_data_"+i).html($(".data_sum_"+i).html());            
    }     
    var data_all = [];
    for (var x = 1; x <= parseInt($("#offset_col").html()); x++) {
      var data = [];
      for (var i = 2; i <= parseInt($(".offset_row").html()); i++) {
        data.push(parseInt($(".row_"+i+"_"+x).html()));
      }      
      redus = data.reduce((a, b) => a + b, 0);
      $(".sum_split_"+x).html(redus);   
      data_all.push(redus);
    }              
    $("#sum_all").html(data_all.reduce((a, b) => a + b, 0));    
    if ($("#point").find(":selected").text().trim() == "Pengunjung" || $("#point").find(":selected").text().trim() == "Pelaku" || $("#point").find(":selected").text().trim() == "Pemberdayaan" ||  $("#point").find(":selected").text().trim() == "Perekonomian") {      
      $(".header_evaluasi").addClass("hide");
      $(".body_evaluasi").addClass("hide");
      $(".colspand_jumlah").attr("colspan", "2")
    }else{
      $(".colspand_jumlah").attr("colspan", "2")
    }
  })  
</script>



 