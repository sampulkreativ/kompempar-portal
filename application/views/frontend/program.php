
<div class="container" style="margin-top:130px;">
    <div class="row">
        <div class="col-md-6  offset-md-3">
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-secondary">
                    <a href="<?=site_url('page/potensi')?>"">Potensi</a>
              </button>
              <button type="button" class="btn btn-secondary">
                <a href="<?=site_url('page/program')?>"">Program</a>
              </button>
              <button type="button" class="btn btn-secondary">Kunjungan</button>
              <button type="button" class="btn btn-secondary">Pelaku Wisata</button>
            </div>
        </div>
    </div>

    <div class="row">
    <table id="example" class="display nowrap" style="width:100%">
        <thead>
            <tr>
                <th style="width:200px;">Jawa Barat</th>
                <th>Kabupaten / Kota</th>
                <th>Bandung</th>
                <th>Bandung Barat</th>
                <th>Kabupaten Bandung</th>
                <th>Kabupaten Bandung</th>
                <th>Kabupaten Bandung</th>
                <th>Kabupaten Bandung</th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Jumlah Program</td>
                <td>16</td>
                <td>8</td>
                <td>5</td>
                <td>3</td>
                <td>3</td>
                <td>3</td>
                <td>3</td>
            </tr>
            <tr>
                <td>Standarisasi</td>
                <td>5</td>
                <td>3</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>3</td>
                <td>3</td>
            </tr>
            <tr>
                <td>Permainan Anak</td>
                <td>6</td>
                <td>3</td>
                <td>2</td>
                <td>1</td>
                <td>1</td>
                <td>3</td>
                <td>3</td>
            </tr>
            <tr>
                <td>Transportasi</td>
                <td>5</td>
                <td>2</td>
                <td>2</td>
                <td>1</td>
               <td>1</td>
                <td>3</td>
                <td>3</td>
            </tr>
            <tr>
                <td>Airi</td>
                <td>Satou</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>33</td>
                <td>1</td>
                <td>3</td>
                <td>3</td>
            </tr>
            
        </tbody>
    </table>
    </div>

</div>