<div class="container" style=" margin-bottom: 80px;">
	<!--<div class="row">
		<div class="col-md-6  offset-md-3">
			<div class="btn-group" role="group" aria-label="Basic example">
			  <button type="button" class="btn btn-secondary">
                    <a href="<?=site_url('page/potensi')?>"">Potensi</a>
              </button>
			  <button type="button" class="btn btn-secondary">
			  	<a href="<?=site_url('page/program')?>"">Program</a>
			  </button>
			  <button type="button" class="btn btn-secondary">Kunjungan</button>
			  <button type="button" class="btn btn-secondary">Pelaku Wisata</button>
			</div>
		</div>
	</div>-->


	<div class="row" style="padding-top:90px;">

    	<div class="col-md-12 ml-md-auto mb-5">
    		<form action="<?php echo base_url("page/kategori/"); ?>" method="get">
    			<div class="row">

    					<div class="col-4">
    						<input type="text" name="keyword" class="form-control" id="inputKeyword" placeholder="Cari objek wisata ...">
    					</div>
    					<div class="col-3 ">
    						<select name="provinsi" id="provinsi" class="form-control custom-select">
							  <option selected>Provinsi</option>
							  	<?php 
									if(!empty($result_provinsi)){
										foreach ($result_provinsi->results->provinces as $value) {
											$id 			= $value->province_id;
											$nama_provinsi  = $value->province_name;
											$desc  			= $value->description;

								?>

							  			<option 
							  			<?php 
							  			if(!empty($history_provinsi)){
							  				if($id == $history_provinsi){
							  					echo " selected ";
							  				}
							  			}
							  			?>
							  			value="<?php echo $id; ?>"><?php echo $nama_provinsi; ?></option>

								<?php
										}
									} else{


									}

							  	?>
							</select>
    					</div>

    					<div class="col-2">
    						<select name="kotakabupaten" id="kotakabupaten" class="kotakabupaten form-control custom-select">
							  <option selected>Kota / Kabupaten</option>
							</select>
    					</div>

    					<div class="col-2">
    						<select name="kecamatan" class="kecamatan form-control custom-select">
							  <option selected>Kecamatan</option>
							</select>
    					</div>


						<div class="col-1">
							<input type="submit" class="btn btn-primary py-2" style="width:100%;" value="cari">
							
						</div>
		            </div>

    		</form>
		            

    				

    	</div>
	</div>

	<div class="row">
				<?php

					if($result_data->status != "ERROR"){
						foreach ($result_data->results->travels as $value) {
							$id 	= $value->id;
							$image 	= $value->photo;
							$judul	= $value->travel_name;
							$desc 	=$value->description;
							//$words = explode(" ",$desc);
							//$show_desc = implode(" ",array_splice($words,0,19))."...";

							$show_desc = substr($desc,0,126)."...";


				?>
							<div class="col-lg-4 col-md-6 ">
					            <div class="product-item">
					              <div class="pigura" style="height:200px;">
					                <img 
					                <?php 
					                		echo "id='myImg$id'";
					                ?>
					                src="<?php echo $image ?>" alt="Image" class="img-fluid" style="height:250px; object-fit: cover; cursor: pointer;" onclick="previewImage(<?php echo $id; ?>)">
					              </div>
					              <div>
					                <h3><a href="<?=site_url("page/objek/$id")?>">
					                	<?php echo $judul; ?>
					                </a></h3>
					                <div class="p-0 mt-3" style="height: 90px; overflow: hidden;">
					                <?php echo $show_desc; ?></div>
					                

					                <!--<div>
					                  <a href="<?=site_url('page/objek')?>" class="btn btn-black mr-1 rounded-0">Cart</a>
					                  <a href="<?=site_url('page/objek')?>" class="btn btn-black btn-outline-black ml-1 rounded-0">View</a>
					                </div>-->
					              </div>
					            </div>
					          </div>



							<!-- The Modal -->
						<div 
							<?php echo "id='previewImage$id'"; ?>
						 class="modalImg">
						  <span <?php echo "class='close close$id'"; ?> >&times;</span>
						  <img class="modal-content" 
							<?php echo "id='img$id'"; ?>>
						  <div id="caption"></div>
						</div>		

				<?php
						} 

						

					}  else{

					?><div class="col-lg-12 col-md-12 mb-5">
						<div class="alert alert-success text-center" role="alert">
						  Objek Wisata Tidak Ditemukan
						</div>
					</div>
					<?php
					}
				?>
	</div>



	<div class="row">
		<div class="col-md-12">

			<nav aria-label="Page navigation example">

				<?php
			    if(isset($links)){
			     echo $links;
			    } 
			    ?>

			  <?php 
			  /*<ul class="pagination justify-content-end">
			    <li class="page-item disabled">
			      <a class="page-link" href="<?=site_url('page/objek')?>" tabindex="-1">Previous</a>
			    </li>
			    <li class="page-item"><a class="page-link" href="<?=site_url('page/objek')?>">1</a></li>
			    <li class="page-item"><a class="page-link" href="<?=site_url('page/objek')?>">2</a></li>
			    <li class="page-item"><a class="page-link" href="<?=site_url('page/objek')?>">3</a></li>
			    <li class="page-item">
			      <a class="page-link" href="<?=site_url('page/objek')?>">Next</a>
			    </li>
			  </ul>*/
			  ?>
			</nav>
		</div>
	</div>

	
</div>


