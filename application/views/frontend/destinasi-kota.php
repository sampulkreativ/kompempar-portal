<div class="container" style="margin-top:130px; margin-bottom: 80px;">
	<div class="row">
		<div class="col-md-6  offset-md-3">
			<div class="btn-group" role="group" aria-label="Basic example">
			  <button type="button" class="btn btn-secondary">
                    <a href="<?=site_url('page/potensi')?>"">Potensi</a>
              </button>
			  <button type="button" class="btn btn-secondary">
			  	<a href="<?=site_url('page/program')?>"">Program</a>
			  </button>
			  <button type="button" class="btn btn-secondary">Kunjungan</button>
			  <button type="button" class="btn btn-secondary">Pelaku Wisata</button>
			</div>
		</div>
	</div>

	<div class="row" style="margin-top:30px; ">
	          <div class="col-lg-4 col-md-6 mb-5">
	            <div class="product-item">
	              <figure>
	                <img src="<?=base_url();?>assets/images/tangkuban-perahu.jpg" alt="Image" class="img-fluid" style="height:300px; object-fit: cover;">
	              </figure>
	              <div class="px-4">
	                <h3><a href="#">Tangkuban Perahu</a></h3>
	                <div class="mb-3">
	                  <span class="meta-icons mr-3"><a href="#" class="mr-2"><span class="icon-star text-warning"></span></a> 5.0</span>
	                  <span class="meta-icons wishlist"><a href="#" class="mr-2"><span class="icon-heart"></span></a> 29</span>
	                </div>
	                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
	                <!--<div>
	                  <a href="#" class="btn btn-black mr-1 rounded-0">Cart</a>
	                  <a href="#" class="btn btn-black btn-outline-black ml-1 rounded-0">View</a>
	                </div>-->
	              </div>
	            </div>
	          </div>

	           <div class="col-lg-4 col-md-6 mb-5">
	            <div class="product-item">
	              <figure>
	                <img src="<?=base_url();?>assets/images/tebing-keraton.jpg" alt="Image" class="img-fluid" style="height:300px; object-fit: cover;">
	              </figure>
	              <div class="px-4">
	                <h3><a href="#">Tebing Keraton</a></h3>
	                <div class="mb-3">
	                  <span class="meta-icons mr-3"><a href="#" class="mr-2"><span class="icon-star text-warning"></span></a> 5.0</span>
	                  <span class="meta-icons wishlist"><a href="#" class="mr-2"><span class="icon-heart"></span></a> 29</span>
	                </div>
	                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
	                <!--<div>
	                  <a href="#" class="btn btn-black mr-1 rounded-0">Cart</a>
	                  <a href="#" class="btn btn-black btn-outline-black ml-1 rounded-0">View</a>
	                </div>-->
	              </div>
	            </div>
	          </div>

	          <div class="col-lg-4 col-md-6 mb-5">
	            <div class="product-item">
	              <figure>
	                <img src="<?=base_url();?>assets/images/telaga-remis.jpg" alt="Image" class="img-fluid" style="height:300px; object-fit: cover;">
	              </figure>
	              <div class="px-4">
	                <h3><a href="#">Telaga Remis</a></h3>
	                <div class="mb-3">
	                  <span class="meta-icons mr-3"><a href="#" class="mr-2"><span class="icon-star text-warning"></span></a> 5.0</span>
	                  <span class="meta-icons wishlist"><a href="#" class="mr-2"><span class="icon-heart"></span></a> 29</span>
	                </div>
	                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
	                <!--<div>
	                  <a href="#" class="btn btn-black mr-1 rounded-0">Cart</a>
	                  <a href="#" class="btn btn-black btn-outline-black ml-1 rounded-0">View</a>
	                </div>-->
	              </div>
	            </div>
	          </div>

	          <div class="col-lg-4 col-md-6 mb-5">
	            <div class="product-item">
	              <figure>
	                <img src="<?=base_url();?>assets/images/maribaya.jpg" alt="Image" class="img-fluid" style="height:300px; object-fit: cover;">
	              </figure>
	              <div class="px-4">
	                <h3><a href="#">Maribaya</a></h3>
	                <div class="mb-3">
	                  <span class="meta-icons mr-3"><a href="#" class="mr-2"><span class="icon-star text-warning"></span></a> 5.0</span>
	                  <span class="meta-icons wishlist"><a href="#" class="mr-2"><span class="icon-heart"></span></a> 29</span>
	                </div>
	                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
	                <!--<div>
	                  <a href="#" class="btn btn-black mr-1 rounded-0">Cart</a>
	                  <a href="#" class="btn btn-black btn-outline-black ml-1 rounded-0">View</a>
	                </div>-->
	              </div>
	            </div>
	          </div>

	           <div class="col-lg-4 col-md-6 mb-5">
	            <div class="product-item">
	              <figure>
	                <img src="<?=base_url();?>assets/images/img_3.jpg" alt="Image" class="img-fluid" style="height:300px; object-fit: cover;">
	              </figure>
	              <div class="px-4">
	                <h3><a href="#">Ciwalk</a></h3>
	                <div class="mb-3">
	                  <span class="meta-icons mr-3"><a href="#" class="mr-2"><span class="icon-star text-warning"></span></a> 5.0</span>
	                  <span class="meta-icons wishlist"><a href="#" class="mr-2"><span class="icon-heart"></span></a> 29</span>
	                </div>
	                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
	                <!--<div>
	                  <a href="#" class="btn btn-black mr-1 rounded-0">Cart</a>
	                  <a href="#" class="btn btn-black btn-outline-black ml-1 rounded-0">View</a>
	                </div>-->
	              </div>
	            </div>
	          </div>

	          <div class="col-lg-4 col-md-6 mb-5">
	            <div class="product-item">
	              <figure>
	                <img src="<?=base_url();?>assets/images/glamping.jpg" alt="Image" class="img-fluid" style="height:300px; object-fit: cover;">
	              </figure>
	              <div class="px-4">
	                <h3><a href="#">Glamping Lakeside</a></h3>
	                <div class="mb-3">
	                  <span class="meta-icons mr-3"><a href="#" class="mr-2"><span class="icon-star text-warning"></span></a> 5.0</span>
	                  <span class="meta-icons wishlist"><a href="#" class="mr-2"><span class="icon-heart"></span></a> 29</span>
	                </div>
	                <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
	                <!--<div>
	                  <a href="#" class="btn btn-black mr-1 rounded-0">Cart</a>
	                  <a href="#" class="btn btn-black btn-outline-black ml-1 rounded-0">View</a>
	                </div>-->
	              </div>
	            </div>
	          </div>
	</div>

	
</div>