<style type="text/css">
	.card-header{padding: 0px !important}
	.table td, .table th{padding: 5px !important}
</style>
<?php $token = $this->session->userdata('token'); ?>
 <?php 
 	if (!empty($result_objek)) {
		$value = $result_objek->results->travel;

		$travel_id 		= $value->id;
		$travel_name 	= $value->travel_name;
		$travel_desc 	= $value->description;

		$travel_cat_id 		= $value->travel_category_id;
		$travel_cat_name 	= $value->travel_category_name;

		$travel_sub_cat_id 		= $value->travel_sub_category_id;
		$travel_sub_cat_name 	= $value->travel_sub_category_name;


		$travel_province_id 	= $value->province_id;
		$travel_province_name 	= $value->province_name;
		$travel_city_id 		= $value->city_id;
		$travel_city_name 		= $value->city_name;


		$travel_village_id 		= $value->village_id;
		$travel_village_name 	= $value->village_name;

		$travel_photo 			= $value->photo;
		
	} 

 ?>

 <div class="site-section bg-light" style="background: #fff !important;">
	      <div class="container">

<!-- 	      	<div class="site-blocks-cover overlay" style="background-image: url(<?php echo $travel_photo?>); min-height:80px !important; height:250px !important;" data-aos="fade" data-stellar-background-ratio="0.5" onclick="previewImage(<?php echo $travel_id; ?>"> -->

	      	<img 
                <?php 
                		echo "id='myImg$travel_id'";
                ?>
                src="<?php echo $travel_photo ?>" alt="Image" class="img-fluid" style="width:1140px; height: 250px; object-fit: cover; cursor: pointer;" onclick="previewImage(<?php echo $travel_id; ?>)">
		     

	        <div class="row mb-3 justify-content-center mt-4">
	          <div class="col-md-6 text-center">
	            <h2 class="section-title mb-3">
	            	<?php 
	            		if(!empty($travel_name)){
	            			echo ucwords($travel_name);
	            		}
	            	?>

	            </h2>
	            <p>
	            	<b>
	            		<?php 
	            			echo $travel_cat_name." - ".$travel_sub_cat_name;
	            		?>
	            	</b>
	            </p>
	            <p>
	            	<?php 
	            		$lokasi = ucwords($travel_village_name.", ".$travel_city_name." - ".$travel_province_name);
	            		echo $lokasi;
	            	?>

	            </p>
	          </div>


	        </div>

	        <div
				<?php echo "id='previewImage$travel_id'"; ?> class="modalImg">
			  <span <?php echo "class='close close$travel_id'"; ?> >&times;</span>
			  <img class="modal-content" 
				<?php echo "id='img$travel_id'"; ?>>
			  <div id="caption"></div>
			</div>

		        <div class="bg-white py-1 mb-4">
		        	<div class="col-md-12 product-title-wrap">
		        		<?php 
		        			if (!empty($travel_desc)) {
		        				echo $travel_desc;
		        			} else{
		        		?>	
		        				<p class="mb-4">
		        					Deskripsi belum tersedia.
		        				</p>
		        		<?php
		        			}
		        		?>
					
					<br>
					<br>


					<?php if(!empty($token)){ ?>					

					<ul class="nav nav-tabs " id="myTab" role="tablist">
						<?php 
						if (!empty($result_objek)) {
							$value = $result_objek->results->travel;
							for ($i=0; $i < sizeof($value->audit_results); $i++) { 
								$nama_audit  =$value->audit_results[$i]->name;
								//$judul_audit = $value->audit_results[$i]->name." Objek Wisata ".ucwords($travel_name);
								$judul_audit = $value->audit_results[$i]->name;

								$collapsed="";
								$show 	  = "";
								if($i==0){
								    $collapsed = "active";								}
						?>

							  	<li class="nav-item ">
							    	 <?php
								      	echo "<a class='nav-link $collapsed' id='".$judul_audit."-tab' data-toggle='tab' href='#".$judul_audit."$i' aria-controls='".$judul_audit."' aria-selected='".$collapsed."'>".$judul_audit."</a>";
								      ?>
							  	</li>

						<?php 
							}
						}
						?>
					</ul>


					<div class="tab-content py-3" id="myTabContent" >
						<?php 
						if (!empty($result_objek)) {
							$value = $result_objek->results->travel;
							for ($i=0; $i < sizeof($value->audit_results); $i++) { 
								$nama_audit  =$value->audit_results[$i]->name;
								//$judul_audit = $value->audit_results[$i]->name." Objek Wisata ".ucwords($travel_name);
								$judul_audit = $value->audit_results[$i]->name;

								$collapsed="";
								$show 	  = "";
								if($i==0){
								    $collapsed = "show active";
								}

								$totalPoint = 0;
									foreach ($value->audit_results[$i]->table_audits as $val) {
									    if(empty($val->auth)){
									        $totalPoint+= $val->point;
									    }
									}

								echo "<div class='tab-pane fade $collapsed' id='".$judul_audit."$i' role='tabpanel' aria-labelledby='".$judul_audit."-tab'>";

						?>
								<table class="table table-striped">
									<thead class="thead-dark">
											<tr>
											    <th scope="col" class="text-center">No</th>
												<th scope="col" class="text-center">Pertanyaan</th>					
												<?php if($judul_audit != 'Pengunjung' && $judul_audit != 'Pemberdayaan' && $judul_audit != 'Pelaku' && $judul_audit != 'Perekonomian'){ ?>
													<th scope="col" class="text-center">Evaluasi</th>
												<?php } ?>
												<th scope="col" class="text-center">Point</th>
											</tr>
											<tr>
												<?php if($judul_audit != 'Pengunjung' && $judul_audit != 'Pemberdayaan' && $judul_audit != 'Pelaku' && $judul_audit != 'Perekonomian'){ ?>
													<th colspan="3" class="text-right">Jumlah Point</th>
												<?php }else{ ?>
													<th colspan="2" class="text-right">Jumlah Point</th>
												<?php } ?>
												<th scope="col" class="text-center">
												    <?php echo $totalPoint; ?></th>
												</tr>
									</thead>

									<tbody>

									        	<?php								        	
									        	if(!empty($value->audit_results[$i]->table_audits)){
									        		if(!empty($value->audit_results[$i]->table_audits[0]->auth)){
									        			echo "
									        				<tr>
														      <th colspan='4' scope='row'>
												        		<div class='alert alert-info' role='alert'>
												        			<a href='".base_url()."sessions/index'>".$val->message."</a>
																</div>
																</th>
															</tr>
									        			";
									        		}else{
									        			$no=1;
									        			//arsort($value->audit_results[$i]->table_audits, $val->point); 
									        			foreach ($value->audit_results[$i]->table_audits as $val) {
									        		
										        			$question 	= $val->question;
										        			if($judul_audit == 'Data'){
										        				$result 	= $val->answer;
										        			}else{
											        			$result 	= $val->evaluation;
											        		}
											        		$point 		= $val->point;	
											        		echo "
											        			<tr>
																  <td>
																  	".$no++."
																  </td>
															      <td scope='row'>
															      	".$question."
															      </td>";
															      if($judul_audit != 'Pengunjung' && $judul_audit != 'Pemberdayaan' && $judul_audit != 'Pelaku' && $judul_audit != 'Perekonomian'){
															      	echo "<td>".$result."</td>";
																  }
																  echo "<td class='text-center'>
															      	".$point."
															    </tr>
											        		";
										        		}
										        	}									        	
									        	} else{
									        	?>
													<tr>
												      <th colspan="4" scope="row">
										        		<div class="alert alert-success" role="alert">
														  Data belum tersedia. 
														</div>
														</th>
													</tr>

									        	<?php
									        	}
									        	?>

									       		</tbody>	
									</table>
						<?php

								echo"</div>";
							}
						?>

					</div>
					<?php 
						}
					?>

				</div>
						<?php
						}
						?>
					
		        	
				

					
		        	</div>
		        </div>
		    </div>

	        <!--<div class="bg-white py-4">
	          <div class="row mx-4 my-4 product-item-2 align-items-start">
	            <div class="col-md-6 mb-5 mb-md-0 order-1 order-md-2">
	              <img src="<?=base_url();?>assets/images/product_1_bg.jpg" alt="Image" class="img-fluid">
	            </div>
	           
	            <div class="col-md-5 mr-auto product-title-wrap order-2 order-md-1">
	              <span class="number">02.</span>
	              <h3 class="text-black mb-4 font-weight-bold">About This Product</h3>
	              <p class="mb-4">Et tempora id nostrum saepe amet doloribus deserunt totam officiis cupiditate asperiores quasi accusantium voluptatum dolorem quae sapiente voluptatem ratione odio iure blanditiis earum fuga molestiae alias dicta perferendis inventore!</p>
	              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus soluta assumenda sed optio, error at?</p>
	              
	              <div class="mb-4"> 
	                <h3 class="text-black font-weight-bold h5">Price:</h3>
	                <div class="price"><del class="mr-2">$269.00</del> $69.00</div>
	              </div>
	              <p>
	                <a href="#" class="btn btn-black btn-outline-black rounded-0 d-block mb-2 mb-lg-0 d-lg-inline-block">View Details</a>
	                <a href="#" class="btn btn-black rounded-0 d-block d-lg-inline-block">Add To Cart</a>
	              </p>
	            </div>
	          </div>
	        </div>-->

	      </div>
	    </div>
