
<div class="container mb-5" >
    <!--<div class="row">
        <div class="col-md-6  offset-md-3">
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-secondary">
                    <a href="<?=site_url('page/potensi')?>">Potensi</a>
              </button>
              <button type="button" class="btn btn-secondary">
                <a href="<?=site_url('page/program')?>">Program</a>
              </button>
              <button type="button" class="btn btn-secondary">Kunjungan</button>
              <button type="button" class="btn btn-secondary">Pelaku Wisata</button>
            </div>
        </div>
    </div>-->

    <div class="row" style="padding-top:90px;">
        <div class="col-md-9 ml-md-auto mb-5">

                    <div class="row">
                        <div class="col-auto ml-auto">
                            <select class="custom-select">
                              <option selected>Potensi</option>
                              <option value="1">One</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                            </select>
                        </div>

                        <div class="col-auto">
                            <select class="custom-select">
                              <option selected>Provinsi</option>
                              <option value="1">One</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                            </select>
                        </div>

                        <div class="col-auto">
                            <select class="custom-select">
                              <option selected>Kabupaten / Kota</option>
                              <option value="1">One</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                            </select>
                        </div>


                        <div class="col-auto">
                            <select class="custom-select">
                              <option selected>Kecamatan</option>
                              <option value="1">One</option>
                              <option value="2">Two</option>
                              <option value="3">Three</option>
                            </select>
                        </div>


                        <div class="col-auto">
                            <button type="button" class="btn btn-primary py-2" style="width:100%;">Cari</button>
                        </div>
                    </div>

                    

        </div>
    </div>

    <div class="row">
        <div class="table-responsive">
            <div class="table-responsive" style="height:300px;">
                                <table class="table">
                                    <thead >
                                        <tr>
                                          <th scope="col" style="width:20% !important;">Jawa Barat</th>
                                          <th scope="col">Bandung </th>
                                          <th scope="col">Cimahi</th>
                                          <th scope="col">Bandung Barat</th>
                                          <th scope="col">Subang</th>
                                          <th scope="col">Bandung </th>
                                          <th scope="col">Cimahi</th>
                                          <th scope="col">Bandung Barat</th>
                                          <th scope="col">Subang</th>
                                          <th scope="col">Bandung </th>
                                          <th scope="col">Cimahi</th>
                                          <th scope="col">Bandung Barat</th>
                                          <th scope="col">Subang</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <th scope="row">Jumlah Potensi</th>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                        </tr>
                                        <tr>
                                          <th scope="row">Apakah ada taman?</th>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                        </tr>


                                        <tr>
                                          <th scope="row">Apakah ada sungai?</th>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                        </tr>
                                        

                                        <tr>
                                          <th scope="row">Apakah akses jalan lancar?</th>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>0</td>
                                          <td>1</td>
                                          <td>1</td>
                                          <td>1</td>
                                        </tr>
                                        
                                      </tbody>
                                </table>
                                
            </div>
        </div>
    
    </div>

</div>