<?php $this->load->helper('Audit'); ?>
<?php if($result_data_objects->results != null){ ?>
  <?php if($result_data_questions->results != null){ ?>              
    <?php $no = ($offset + 1);foreach ($result_data_questions->results->questions as $question) { ?>                  
      <tr>
        <td><?php echo $no++ ?></td>
        <td style="white-space:nowrap;"><?php echo $question->question ?></td>
        <td style="white-space:nowrap;"><?php echo $question->evaluation ?></td>
                        
          <?php foreach ($result_data_objects->results->objects as $object) { ?>                  
            <td>                        
              <?php echo find_audit_results($question->id, $object->id, $object->type) ?>        
            </td>                  
          <?php }?>                          
      </tr>                  
    <?php }?>
  <?php }?> 
<?php }else{?> 
	<tr>
		<td colspan="100"> Data tidak ditemukan</td>
	</tr>
<?php } ?>