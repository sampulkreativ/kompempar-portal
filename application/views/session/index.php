<style type="text/css">
	.forget a{
		text-decoration: none;
		color: #49666f;
	}
	.forget a:hover{
		text-decoration: underline;
	}
</style>

<div class="login-form col-lg-3" style="background-color: #fff">
	<center>
		<img src="<?php echo base_url('assets/images/logo-kompepar.jpg')?>" class="img-fluid" alt="Responsive image" style="max-width:180px;">
	</center>
	<h1 class="login">Masuk</h1>
	<form action="<?=base_url();?>sessions/create" method="post">
		<?php if(isset($alert)){?>
			<div class="alert alert-danger">
				<?php echo $alert ?>
			</div>
			<br>
		<?php } ?>
		<div class="form-group-login">
			<input type="text" name="username" class="form-control-login data-input" placeholder="Email" data-required="true">
			<i class="fa fa-user"></i>
		</div>
		<div class="form-group-login log-status">
			<input type="password" name="password" class="form-control-login data-input" placeholder="Kata Sandi" data-required="true">
			<i class="fa fa-lock"></i>
		</div>
		<button  class="btn btn-primary btn-block">Masuk</button><br>
		<center class="forget"><a href="<?= base_url(); ?>">Beranda</a> | <a href="<?= base_url(); ?>sessions/register">Daftar</a></center>
	</form>
	<p class="login-footer">&copy; 2019. Komite Pengerak Pariwisata Jawa Barat</p>
</div>

