<style type="text/css">
	.forget a{
		text-decoration: none;
		color: #49666f;
	}
	.forget a:hover{
		text-decoration: underline;
	}
</style>

<div class="login-form col-lg-3">
	<center><img src="<?= base_url(); ?>assets/images/logo.png" width="40%"></center>
	<h1 class="login">Atur Kata Sandi</h1>
	<form action="<?=base_url();?>sessions/process_change_password/<?php echo $key ?>" method="post">
		<div id="notice_js">
			<?php if(isset($alert)){?>
				<div class="alert alert-danger">
					<?php echo $alert ?>
				</div>
				<br>
			<?php } ?>
		</div>
		<div class="form-group-login">
			<input type="password" name="old_password" class="form-control-login data-input old_password" placeholder="Sandi Baru" data-required="true">
			<i class="fa fa-lock"></i>
		</div>
		<div class="form-group-login log-status">
			<input type="password" name="new_password" class="form-control-login data-input new_password" placeholder="Konfirmasi Sandi" data-required="true">
			<i class="fa fa-lock"></i>
		</div>
		<button  class="btn btn-primary btn-block btn_confirm_password" >Kirim</button>
	</form>
	<p class="login-footer">&copy; 2017. Akademi Fisioterai Rumah Sakit Dustira</p>
</div>