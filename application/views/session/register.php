<style type="text/css">
	.forget a{
		text-decoration: none;
		color: #49666f;
	}
	.forget a:hover{
		text-decoration: underline;
	}
</style>

<div class="login-form col-lg-3" style="background-color: #fff">
	<center>
		<img src="<?php echo base_url('assets/images/logo-kompepar.jpg')?>" class="img-fluid" alt="Responsive image" style="max-width:180px;">
	</center>
	<h1 class="login">Daftar Anggota</h1>
	<form action="<?=base_url();?>sessions/create_register" method="post">
		<?php if(isset($alert)){?>
			<div class="alert alert-danger">
				<?php echo $alert ?>
			</div>
			<br>
		<?php } ?>
		<?php if(isset($notice)){?>
			<div class="alert alert-success">
				<?php echo $notice ?>
			</div>
			<br>
			<center class="forget"><a href="<?= base_url(); ?>">Beranda</a></center>
		<?php }else{ ?>
			<div class="form-group-login">
				<input type="text" name="full_name" class="form-control-login data-input" placeholder="Nama Lengkap" data-required="true">
				<i class="fa fa-user"></i>
			</div>
			<div class="form-group-login">				
				<input type="email" name="email" class="form-control-login data-input" placeholder="Email" data-required="true">
				<i class="fa fa-mail"></i>
			</div>
			<div class="form-group-login log-status">
				<input type="password" name="password" class="form-control-login data-input" placeholder="Kata Sandi" data-required="true">
				<i class="fa fa-lock"></i>
			</div>
			<button  class="btn btn-primary btn-block">Daftar</button><br>
			<center class="forget"><a href="<?= base_url(); ?>">Beranda</a> | <a href="<?= base_url(); ?>sessions/">Masuk</a></center>
		<?php } ?>
	</form>
	<p class="login-footer">&copy; 2019. Komite Pengerak Pariwisata Jawa Barat</p>
</div>

