<style type="text/css">
	.forget a, .forget span{
		text-decoration: none;
		color: #49666f;
	}
	.forget a:hover{
		text-decoration: underline;
	}
</style>

<div class="login-form col-lg-3">
	<center><img src="<?= base_url(); ?>assets/images/logo.png" width="40%"></center>
	<h1 class="login">Lupa Kata Sandi</h1>
	<form action="<?=base_url();?>sessions/process_forget" method="post">
		<?php if(isset($alert)){?>
			<div class="alert alert-danger">
				<?php echo $alert ?>
			</div>
			<br>
		<?php } ?>
		<?php if(isset($notice)){?>
			<div class="alert alert-success">
				<?php echo $notice ?>
			</div>
			<br>
		<?php } ?>
		<div class="form-group-login">
			<input type="text" name="email" class="form-control-login data-input" placeholder="Masukan Email Anda" data-required="true">		
		</div>
		<button  class="btn btn-primary btn-block" >Kirim</button>
		<br>
		<center class="forget"><a href="<?= base_url(); ?>login">Kembali</a></center>
	</form>
	<p class="login-footer">&copy; 2017. Akademi Fisioterai Rumah Sakit Dustira</p>
</div>
