<div class="row">
	<div class="col-lg-12">
		<?php $this->load->view('shared/errors'); ?>
		<ul class="nav nav-tabs">
			<li class="<?php echo isset($_GET['history']) == 0 ? 'active' : '' ?>">
				<a href="#home" data-toggle="tab" aria-expanded="true">
					<span class="visible-xs"><i class="fa fa-user"></i></span>
					<span class="hidden-xs">FRS Perwalian</span>
				</a>
			</li>
			<li class="<?php echo isset($_GET['history']) == 0 ? '' : 'active' ?>">
				<a href="#message" data-toggle="tab" aria-expanded="true">
					<span class="visible-xs"><i class="fa fa-home"></i></span>
					<span class="hidden-xs">Riwayat FRS</span>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane <?php echo isset($_GET['history']) == 0 ? 'active' : '' ?>" id="home">
			<h3>FRS Perwalian</h3>
				<?php if($result_array->status != "OK"){ ?>
					<h4>Belum Ada Jadwal Perwalian!</h4>
				<?php }else{ ?>
					<h4 class="col-lg-8">Jadwal Perwalian untuk periode <?php echo $result_data->periode->mulai ?> s/d <?php echo $result_data->periode->akhir ?></h4>
					<span class="pull-right col-lg-2">
						<a href="<?php echo base_url() ?>study_plan?download=pdf" class="btn btn-danger">Download PDF</a>
					</span>
					<form method="post" action="<?php echo base_url() ?>study_plan/create">
						<table id="datatable" class="table table-striped table-bordered">
							<thead>
							    <tr>
							        <th width="2%">No</th>
							        <th width="12%">No. Matkul</th>
							        <th width="50%">Matakuliah</th>
							        <th width="3%">SKS</th>
							        <th width="10%">Status</th>
							        <th width="10%">Aksi</th>
							    </tr>
							</thead>
							<tbody>
								<?php $no = 1;foreach ($result_array_krs->matkul as $matkul) { ?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $matkul->id ?></td>
										<td><?php echo $matkul->nama ?></td>
										<td><?php $sks[]= $matkul->sks; echo $matkul->sks ?></td>
										<?php if($matkul->status == 2){
											$status = 'Mundur';
										}else if($matkul->status == 1){
											$status = 'Diambil';
										}else{
											$status = 'Belum diambil';
										} ?>
										<td><?php echo $status ?></td>
										<td>											
											<?php if($matkul->status == 0){ ?>
												<label><input type="checkbox" value="<?php echo $matkul->id ?>" name="matkul[]"> Ambil</label>
											<?php }else if($matkul->status != 2){ ?>
												<a href="<?php echo base_url() ?>study_plan/backward/<?php echo $matkul->id ?>" class="btn btn-white" title="Mundur"><b class="ti-back-left"></b></a>
												<label> Mundur</label>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="2">
										
									</th>
									<th class="pull-right">
										<span>Total SKS:</span>
										<?php echo array_sum($sks) ?>
									</th>
									<th colspan="2">
										<input type="submit" name="simpan" value="simpan" class="pull-right btn btn-success">
									</th>									
								</tr>
							</tfoot>
						</table>
					</form>
				<?php } ?>
			</div>
			<div class="tab-pane <?php echo isset($_GET['history']) == 0 ? '' : 'active' ?>" id="message">
				<h3>Riwayat FRS</h3>
				<div class="col-lg-2" style="padding-left:0px">
					<form class="form-horizontal" action="<?=base_url();?>study_plan/index" method="get">
						<input type="hidden" name="history" value="true">
						<div class="form-group input-group" style="margin:0">
							<select name="semester" class="form-control data-input" style="width:200px" data-required="true">
								<option value="">-- Pilih Semester --</option>
								<option value="1" <?php echo isset($_GET['semester']) && $_GET['semester'] == '1' ? 'selected' : '' ?>>Semester 1</option>
								<option value="2" <?php echo isset($_GET['semester']) && $_GET['semester'] == '2' ? 'selected' : '' ?>>Semester 2</option>
								<option value="3" <?php echo isset($_GET['semester']) && $_GET['semester'] == '3' ? 'selected' : '' ?>>Semester 3</option>
								<option value="4" <?php echo isset($_GET['semester']) && $_GET['semester'] == '4' ? 'selected' : '' ?>>Semester 4</option>
								<option value="5" <?php echo isset($_GET['semester']) && $_GET['semester'] == '5' ? 'selected' : '' ?>>Semester 5</option>
								<option value="6" <?php echo isset($_GET['semester']) && $_GET['semester'] == '6' ? 'selected' : '' ?>>Semester 6</option>
							</select>
							<span class="input-group-btn">
								<?php echo form_submit(array('class' => 'btn btn-warning', 'value' => 'Cari')); ?>
							</span>
						</div>
					</form>
				</div>
				<div class="clearfix"></div><br>
				<?php if (isset($result_histories) == 0){ ?>
					<h4>Silakan pilih semester terlebih dahulu</h4>
				<?php } else { ?>
				<!-- <h4>Belum Ada Jadwal Perwalian untuk Tahun Akademik : 2017/2018 SEMESTER : GANJIL !</h4> -->
					<table id="" class="table table-striped table-bordered">
						<thead>
						    <tr>
								<th width="3%">No</th>
								<th width="60%">Matakuliah</th>
								<th width="3%">SKS</th>
								<th width="10%">Status</th>
								<th width="10%">Pengambilan</th>
						    </tr>
						</thead>
						<tbody>
							<?php if($result_histories != null){?>
								<?php $no = 1;foreach ($result_histories->krs as $matkul) { ?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $matkul->matakuliah ?></td>
										<td><?php $sks_history[]= $matkul->sks; echo $matkul->sks ?></td>
										<td><?php echo $matkul->status_persetujuan ?></td>
										<td><?php echo $matkul->status_pengambilan ?></td>
									</tr>
								<?php } ?>
							<?php }else{?>
								<tr>
									<th colspan="5">Tidak ada data</th>
								</tr>
							<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<th colspan="3">
									<span class="pull-right">Total SKS: <?php echo array_sum(isset($sks_history) ? $sks_history : [0]) ?></span>
								</th>
							</tr>
						</tfoot>
					</table>
				<?php } ?>
			</div>
		</div>
	</div>
</div>