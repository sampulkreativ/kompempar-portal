<div style="position: absolute;"><img src="<?= base_url(); ?>assets/images/logo.png" width="90px"></div>
<div style="text-align: center">
	<h3>Rencana Pembelajaran</h3>
	<h4>FRS Perwalian</h4>
	<h4 style="margin-top: -10px; font-weight: normal">(<?= $this->session->userdata('nim') ?>) <?= $this->session->userdata('nama') ?></h4>
</div>	
	<?php if($result_array->status != "OK"){ ?>
		<h4>Belum Ada Jadwal Perwalian!</h4>
	<?php }else{ ?>
		<h4 style="text-align: center">Jadwal Perwalian untuk periode <?php echo $result_data->periode->mulai ?> s/d <?php echo $result_data->periode->akhir ?></h4>
		<form method="post" action="<?php echo base_url() ?>study_plan/create">
			<table id="datatable" class="table table-striped table-bordered" style=" border-collapse: collapse;">
				<thead>
				    <tr>
				        <th style="border: 1px solid grey" width="5%"><b>No</b></td>
				        <th style="border: 1px solid grey" width="60%"><b>Matakuliah</b></td>
				        <th style="border: 1px solid grey" width="5%">SKS</th>
				        <th style="border: 1px solid grey" width="10%">Status</th>
				    </tr>
				</thead>
				<tbody>
					<?php $no = 1;foreach ($result_array_krs->matkul as $matkul) { ?>
						<tr>
							<td style="border: 1px solid grey"><?php echo $no++ ?></td>
							<td style="border: 1px solid grey"><?php echo $matkul->nama ?></td>
							<td style="border: 1px solid grey"><center><?php $sks[]= $matkul->sks; echo $matkul->sks ?></center></td>
							<?php if($matkul->status == 2){
								$status = 'Mundur';
							}else if($matkul->status == 1){
								$status = 'Diambil';
							}else{
								$status = 'Belum diambil';
							} ?>
							<td style="border: 1px solid grey"><center><?php echo $status ?></center></td>
						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="2" style="border: 1px solid grey">
							<b style="text-align: right">Total SKS:</span></b>
						</td>
						<th style="border: 1px solid grey"><?php echo array_sum($sks) ?></th>
						<th style="border: 1px solid grey"></th>
					</tr>
				</tfoot>
			</table>
		</form>
	<?php } ?>
</div>