<div class="row">
	<div class="col-lg-12">
		<?php $this->load->view('shared/errors'); ?>
		<ul class="nav nav-tabs"></ul>
		<div class="tab-content">		
			<form class="form-horizontal" action="<?=base_url();?>evaluation/create" method="post">	
				<label>Silakan isi daftar evaluasi berikut ini:</label>			
				<br><br>
				<div class="tab-pane <?php echo isset($_GET['history']) == 0 ? 'active' : '' ?>" id="home">
				<h3>FRS Perwalian</h3>
					<?php if($result_data->status != "OK"){ ?>
						<h4>Belum Ada Evaluasi</h4>
					<?php }else{ ?>														
						<?php $no=1; foreach ($result_data->results->evaluasi as $evaluation){?>
							<input type="hidden" name="evaluation[]" value="<?php echo $evaluation->id ?>">
							<div class="panel-group" id="accordion_<?php echo $evaluation->id ?>">
							    <div class="panel panel-default">
							      <div class="panel-heading">
							        <h4 class="panel-title">
							          <a data-toggle="collapse" data-parent="#accordion_<?php echo $evaluation->id ?>" href="#collapse_<?php echo $evaluation->id ?>"><?php echo $evaluation->judul ?></a>
							        </h4>
							      </div>
							      <div id="collapse_<?php echo $evaluation->id ?>" class="panel-collapse collapse">
							        <div class="panel-body">
							        	<ul style="list-style: none;padding: 0px 0px">
							        		<?php $no=1; foreach ($evaluation->pilihan as $option){?>

								        		<li style="padding: 10px">
								        			<input type="radio" name="pilihan_<?php echo $evaluation->id ?>" value="<?php echo $option->id ?>" <?php echo $evaluation->jawaban == $option->id ? "checked" : "" ?>> &nbsp;<?php echo $option->keterangan ?>
								        		</li>
								        	<?php } ?>								        	
							        	</ul>
							    	</div>
							      </div>
							    </div>    
							</div>						
						<?php } ?>
					<?php } ?>
					<input type="submit" name="simpan" value="simpan" class="btn btn-success">
				</div>		
			</form>	
		</div>
	</div>
</div>