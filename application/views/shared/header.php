<div class="topbar-main">
	<div class="container">
		<!-- Logo container-->
		<div class="logo">
			<a href="index.html" class="logo" id="logo">
				<!-- <center> -->
					<div class="col-lg-1 col-md-1">
						<img src="<?= base_url(); ?>assets/images/logo.png" >
					</div>
					<div class="col-lg-9 col-md-9">
						<span>MAHASISWA AKFIS RS. DUSTIRA</span>
					</div>
				<!-- </center> -->
			</a>
		</div>

		<div class="menu-extras">
			<ul class="nav navbar-nav navbar-right pull-right logo-text">
				<li class="logo" style="font-size:12px; margin-top:25px">(<?= $this->session->userdata('nim') ?>) <?= $this->session->userdata('nama') ?></li>
				<li class="dropdown navbar-c-items">
						<a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true"><img src="<?php echo $this->session->userdata('foto') == '' ? base_url().'/assets/images/missing.jpg' : $this->session->userdata('foto') ?>" alt="user-img" class="img-circle"> </a>
						<ul class="dropdown-menu">
								<li><a href="<?=base_url();?>setting"><i class="fa fa-gear text-custom m-r-10"></i> Pengaturan</a></li>
								<li class="divider"></li>
								<li><a href="<?=base_url();?>logout"><i class="fa fa-power-off text-danger m-r-10"></i> Keluar</a></li>
						</ul>
				</li>
			</ul>
			<div class="menu-item">
				<!-- Mobile menu toggle-->
				<a class="navbar-toggle">
					<div class="lines">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>