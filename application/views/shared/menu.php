<div class="navbar-custom">
     <div class="container">
          <div id="navigation">
              <!-- Navigation Menu-->
              <ul class="navigation-menu">
                  <li class="has-submenu <?php echo isset($home) ? $home : "" ?>">
                      <a href="<?=base_url();?>welcome"><center><img class="md" src="<?=base_url();?>assets/images/home.png"><div class="clearfix"></div>Beranda</center></a>
                  </li>
                  <li class="has-submenu <?php echo isset($finance) ? $finance : "" ?>">
                      <a href="<?=base_url();?>finance"><center><img class="md" src="<?=base_url();?>assets/images/finance.png"><div class="clearfix"></div>Keuangan</center></a>
                  </li>
                  <li class="has-submenu <?php echo isset($result_study) ? $result_study : "" ?>">
                      <a href="<?=base_url();?>study"><center><img class="md" src="<?=base_url();?>assets/images/study.png"><div class="clearfix"></div>Hasil Belajar</center></a>
                  </li>
                  <li class="has-submenu <?php echo isset($schedule) ? $schedule : "" ?>">
                      <a href="<?=base_url();?>schedule?schedule=tugas&semester_tugas=<?php echo $this->session->userdata('semester') ?>&tanggal=<?php echo date('Y-m-d') ?>"><center><img class="md" src="<?=base_url();?>assets/images/schedule.png"><div class="clearfix"></div>Jadwal</center></a>
                  </li>
                  <li class="has-submenu <?php echo isset($active_evaluation) ? $active_evaluation : "" ?>">
                      <a href="<?=base_url();?>evaluation"><center><img class="md" src="<?=base_url();?>assets/images/evaluation.png"><div class="clearfix"></div>Evaluasi</center></a>
                  </li>
                  <li class="has-submenu <?php echo isset($active_krs) ? $active_krs : "" ?>">
                      <a href="<?=base_url();?>study_plan"><center><img class="md" src="<?=base_url();?>assets/images/frs.png"><div class="clearfix"></div>Rencana Belajar</center></a>
                  </li>
                  <li class="has-submenu <?php echo isset($setting) ? $setting : "" ?>">
                      <a href="<?=base_url();?>setting"><center><img class="md" src="<?=base_url();?>assets/images/setting.png"><div class="clearfix"></div>Pengaturan</center></a>
                  </li>

                  <li class="has-submenu">
                      <a href="<?=base_url();?>logout"><center><img class="md" src="<?=base_url();?>assets/images/logout.png"><div class="clearfix"></div>Keluar</center></a>
                  </li>
              </ul>
              <!-- End navigation menu        -->
          </div>
      </div> <!-- end container -->
  </div>