<?php if (isset($errors)){ ?>
	<div class="alert alert-dismissable alert-danger">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<?php foreach ($errors as $error) {?>
			<strong><?php echo $error ?></strong>
		<?php } ?>
	</div>
<?php } ?>

<?php if (isset($_GET['notice'])){ ?>
	<div class="alert alert-dismissable alert-notice">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong><?php echo $_GET['notice'] ?></strong>
	</div>
<?php } ?>