<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<?php 
			$newTitle = "";
			if (!empty($title)) {
				$newTitle = $title;
			} else if ($this->uri->segment(2) == "objek") {
				
				if (!empty($result_objek)) {
					$value = $result_objek->results->travel;

					$travel_id 		= $value->id;
					$travel_name 	= $value->travel_name;

					$newTitle = "Kompepar | ".$travel_name;

					
				} else{
					$newTitle = "Objek Wisata tidak ditemukan.";
				}

			} else{
				$newTitle = "Kompepar - Kelompok Penggerak Parawisata";
			}
		?>
		<title>
			<?php
				echo ucwords($newTitle);
			?>

		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Kompepar - Kelompok Penggerak Parawisata">
		<meta name="author" content="Sampulkreativ Technology">

		<link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
		<link rel="stylesheet" href="<?=base_url();?>assets/fonts/icomoon/style.css">

   		<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css">
		<!--<link href="<?=base_url();?>assets/stylesheet/login.css" rel="stylesheet">
		<link href="<?=base_url();?>assets/stylesheet/style.css" rel="stylesheet">-->

		<link rel="stylesheet" href="<?=base_url();?>assets/css/jquery-ui.css">
	    <link rel="stylesheet" href="<?=base_url();?>assets/css/owl.carousel.min.css">
	    <link rel="stylesheet" href="<?=base_url();?>assets/css/owl.theme.default.min.css">

	    <link rel="stylesheet" href="<?=base_url();?>assets/css/jquery.fancybox.min.css">

	    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-datepicker.css">

	    <link rel="stylesheet" href="<?=base_url();?>assets/fonts/flaticon/font/flaticon.css">

	    <link rel="stylesheet" href="<?=base_url();?>assets/css/aos.css">

	    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

	    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
 


	    <style type="text/css">
	    	.navbar-nav li:hover > ul.dropdown-menu {
	    		margin-top:-5px;
			    display: block;
			}
			.dropdown-submenu {
			    position:relative;
			}
			.dropdown-submenu>.dropdown-menu {
			    top:0;
			    left:100%;
			    margin-top:-6px;
			}

			/* rotate caret on hover */
			.dropdown-menu > li > a:hover:after {
			    text-decoration: underline;
			    transform: rotate(-90deg);
			} 

			div.dataTables_wrapper {
		        width: 100%;
		        margin: 0 auto;
		        //padding:20px;
		    }


			/* The Modal (background) */
			.modalImg {
			  display: none; /* Hidden by default */
			  position: fixed; /* Stay in place */
			  z-index: 999999; /* Sit on top */
			  padding-top: 100px; /* Location of the box */
			  left: 0;
			  top: 0;
			  width: 100%; /* Full width */
			  height: 100%; /* Full height */
			  overflow: auto; /* Enable scroll if needed */
			  background-color: rgb(0,0,0); /* Fallback color */
			  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
			}

			/* Modal Content (image) */
			.modal-content {
			  margin: auto;
			  display: block;
			  width: 80%;
			  max-width: 700px;
			}

			/* Caption of Modal Image */
			#caption {
			  margin: auto;
			  display: block;
			  width: 80%;
			  max-width: 700px;
			  text-align: center;
			  color: #ccc;
			  padding: 10px 0;
			  height: 150px;
			}

			/* Add Animation */
			.modal-content, #caption {  
			  -webkit-animation-name: zoom;
			  -webkit-animation-duration: 0.6s;
			  animation-name: zoom;
			  animation-duration: 0.6s;
			}

			@-webkit-keyframes zoom {
			  from {-webkit-transform:scale(0)} 
			  to {-webkit-transform:scale(1)}
			}

			@keyframes zoom {
			  from {transform:scale(0)} 
			  to {transform:scale(1)}
			}

			/* The Close Button */
			.close {
			  position: absolute;
			  top: 15px;
			  right: 35px;
			  color: #f1f1f1;
			  font-size: 40px;
			  font-weight: bold;
			  transition: 0.3s;
			}

			.close:hover,
			.close:focus {
			  color: #bbb;
			  text-decoration: none;
			  cursor: pointer;
			}

			/* 100% Image Width on Smaller Screens */
			@media only screen and (max-width: 700px){
			  .modal-content {
			    width: 100%;
			  }
			}
	    </style>

		<!--<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
		<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>-->
	</head>
	<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
	<div class="site-wrap">
	    <?php 

	    /*<div class="site-mobile-menu site-navbar-target">
	      <div class="site-mobile-menu-header">
	        <div class="site-mobile-menu-close mt-3">
	          <span class="icon-close2 js-menu-toggle"></span>
	        </div>
	      </div>
	      <div class="site-mobile-menu-body"></div>
	    </div>
	   
	    <div class="top-bar py-3 bg-light" id="home-section">
	      <div class="container">
	        <div class="row align-items-center">
	         
	          <div class="col-6 text-left">
	            <ul class="social-media">
	              <li><a href="#" class=""><span class="icon-facebook"></span></a></li>
	              <li><a href="#" class=""><span class="icon-twitter"></span></a></li>
	              <li><a href="#" class=""><span class="icon-instagram"></span></a></li>
	              <li><a href="#" class=""><span class="icon-linkedin"></span></a></li>
	            </ul>
	          </div>
	          <div class="col-6">
	            <p class="mb-0 float-right">
	              <span class="mr-3"><a href="tel://#"> <span class="icon-phone mr-2" style="position: relative; top: 2px;"></span><span class="d-none d-lg-inline-block text-black">(+022) 234 5678 9101</span></a></span>
	              <span><a href="#"><span class="icon-envelope mr-2" style="position: relative; top: 2px;"></span><span class="d-none d-lg-inline-block text-black">contact@kompepar.com</span></a></span>
	            </p>
	            
	          </div>
	        </div>
	      </div> 
	    </div>*/

	    ?>


	    <header class="site-navbar bg-white js-sticky-header site-navbar-target" role="banner">

	      <div class="container py-2">
	        <div class="row align-items-center">


	          <div class="col-12 col-md-12"> <!-- class d-none d-xl-block -->

	          	<nav class="navbar navbar-expand-md navbar-light px-0">
				  	<h1 class="mb-0 site-logo">
				  		<a href="<?=site_url('')?>" class="text-black mb-0">

				  			<img src="<?php echo base_url('assets/images/logo-kompepar.jpg')?>" class="img-fluid" alt="Responsive image" style="max-width:180px;">

				  			<!--Kompepar<span class="text-primary">.</span>--> 
				  		</a>
				  	</h1>
				  
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarNavDropdown">
				    <ul class="navbar-nav ml-auto" style="font-size:11pt !important;">
							<li class="nav-link">
							    <a class="nav-link" href="<?=site_url()?>">Beranda</a>
							</li>

						<?php
							if(!empty($result_menu)){
								foreach ($result_menu->results->travel_categories as $value) {
											if(!empty($value->sub_travel_category)){
												$id_kategori = $value->id;
												$nama_kategori = $value->category_name;
										?>
												<li class="nav-link dropdown ">
											        <a class="nav-link dropdown-toggle" href="#" 
											        <?php 
											        	echo "id='navbarDropdownMenu$nama_kategori'";
											        ?>
											         data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											          <?php echo $nama_kategori;  ?>
											        </a>
											        <ul class="dropdown-menu" 
											        <?php 
											        	echo "aria-labelledby='navbarDropdownMenu$nama_kategori'";
											        ?>

											        >
											        	<?php 
											        		foreach ($value->sub_travel_category as $value2) {
																$id_sub_kategori = $value2->id;
																$nama_sub_kategori = strtolower($value2->sub_travel_category_name);

											        	?>

													        	<li ><a class="dropdown-item" href="<?=site_url("page/kategori/$nama_sub_kategori/$id_kategori/$id_sub_kategori")?>">
													        		<?php echo ucfirst($nama_sub_kategori); ?>
													        	</a>
													        	</li>

											        	<?php
											        		}
											        	?>
											        </ul>
											    </li>

										<?php
											} else{
										?>
												<a class="nav-link" href="<?=site_url("page/destinasi/$value->id")?>">
											    	<?php echo $value->category_name;  ?>
											    </a>
										<?php
											}
										?>
						<?php
								}

							}
						?>



					        <li class="nav-link">
						       <a class="nav-link" href="#">Hubungi Kami <span class="sr-only">(current)</span></a>
						    </li>

						    
						    	<?php if(!!empty($token)){ ?>
						       	<?php }else{ ?>						       		
						       		<li class="nav-link">
						       			<a class="nav-link" href="<?=site_url("logout")?>">Keluar</a>
						       		</li>
						       	<?php } ?>
						    </li>

				       


				     
				    </ul>
				  </div>
				</nav>

				<?php
	            /*<nav class="site-navigation position-relative  navbar navbar-expand  text-right" style="display: contents;" role="navigation">
	            	<ul class="navbar-nav site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
	                <li><a href="#home-section" class="nav-link">Destinasi</a></li>
	                <li><a href="#products-section" class="nav-link">Industri</a></li>
	                <li><a href="#products-section" class="nav-link">Promosi</a></li>
	                <li><a href="#about-section" class="nav-link">Tentang Kami</a></li>
	                <li><a href="#special-section" class="nav-link">Kelembagaan</a></li>
	                <li><a href="#testimonials-section" class="nav-link">Kontak</a></li>
	                <li class="nav-item dropdown">
			        <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			          Dropdown
			        </a>
			        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			          <li><a class="dropdown-item" href="#">Action</a></li>
			          <li><a class="dropdown-item" href="#">Another action</a></li>
			          <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="http://google.com">Google</a>
			            <ul class="dropdown-menu">
			              <li><a class="dropdown-item" href="#">Submenu</a></li>
			              <li><a class="dropdown-item" href="#">Submenu0</a></li>
			              <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Submenu 1</a>
			                <ul class="dropdown-menu">
			                  <li><a class="dropdown-item" href="#">Subsubmenu1</a></li>
			                  <li><a class="dropdown-item" href="#">Subsubmenu1</a></li>
			                </ul>
			              </li>
			              <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Submenu 2</a>
			                <ul class="dropdown-menu">
			                  <li><a class="dropdown-item" href="#">Subsubmenu2</a></li>
			                  <li><a class="dropdown-item" href="#">Subsubmenu2</a></li>
			                </ul>
			              </li>
			            </ul>
			          </li>
			        </ul>
			      </li>
	              
	              </ul>
	            </nav>*/
	            ?>
	          </div>


	          <?php
	            /*<div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;">
	          	<a href="#" class="site-menu-toggle text-black float-right"  type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
	          			<span class="icon-menu h3"></span>
	          		</a>
	          	</div>*/
	          	?>

	        </div>
	      </div>
	      
	    </header>


		<?php if($middle) echo $middle; ?>

  	</div> <!-- .site-wrap -->

  	 <footer class="site-footer bg-white" style="background-color:#111 !important; padding:2em 0em;">
	      <div class="container">
	        <div class="row">
	          <div class="col-md-12">
	            <div class="row">
	              <div class="col-md-5">
	                <h2 class="footer-heading mb-4">Tentang Kami</h2>
	                <p></p>
	              </div>
	              <div class="col-md-4 ">
	                <h2 class="footer-heading mb-4">Menu Utama</h2>
	                 <ul class="list-unstyled">
	                  <li><a href="/">Beranda</a></li>
	                  <?php if(!empty($token)){ ?>
		                  <li>
			       			<a href="http://member.kompepar.or.id/members?z=<?php echo $this->session->userdata('token') ?>&y=si89ajh<?php echo $this->session->userdata('token') ?>gha6sds">Area Anggota</a>
			       		  </li>
			       		  <li>
		                  	<a href="http://member.kompepar.or.id/members/audit_result?z=<?php echo $this->session->userdata('token') ?>&y=si89ajh<?php echo $this->session->userdata('token') ?>gha6sds">Hasil Audit</a>
		                  </li>
			       	  <?php } ?>
	                  <li><a href="#">Blog</a></li>
	                  <li><a href="#">Tentang Kami</a></li>	                  

	                 

						<?php if(!!empty($token)){ ?>
							<li> 
								<a href="<?=site_url("sessions/")?>">Masuk/Login</a>
							</li>
						<?php }else{ ?>						       		
							<!--<li>
						       	<a href="<?=site_url("logout")?>">Keluar</a>
						    </li>-->
							<?php } ?>


	                </ul>

	                
	              </div>
	              <div class="col-md-3">
	                <h2 class="footer-heading mb-4">Follow Us</h2>
	                <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
	                <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
	                <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
	                <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
	              </div>
	            </div>
	          </div>
	          <!--<div class="col-md-3 ml-auto">
	            <h2 class="footer-heading mb-4">Featured Product</h2>
	            <a href="#"><img src="<?=base_url();?>assets/images/product_1_bg.jpg" alt="Image" class="img-fluid mb-3"></a>
	            <h4 class="h5">Leather Brown Shoe</h4>
	            <strong class="text-black mb-3 d-inline-block">$60.00</strong>
	            <p><a href="#" class="btn btn-black rounded-0">Add to Cart</a></p>
	          </div>-->
	        </div>
	        <!--<div class="row pt-5 mt-5 text-center">
	          <div class="col-md-12">
	            <div class="border-top pt-5">
	            <p>
	        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
	      </p>
	            </div>
	          </div>
	          
	        </div>-->
	      </div>
	    </footer>


	</body>

	<!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>-->

  	<script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/form-validation.js"></script>
	
	<script  type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-migrate-3.0.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
	<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.stellar.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.countdown.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.easing.1.3.js"></script>
	<script src="<?php echo base_url();?>assets/js/aos.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.fancybox.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.sticky.js"></script>

	  
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/main.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		    $('#example').DataTable( {
		        "scrollY": 200,
		        "scrollX": true
		    } );
		} );
	</script>


<script type="text/javascript">
    $(document).ready(function(){

		var cekHalaman =<?php echo json_encode($this->uri->segment(2)); ?>;
		
		if(<?php echo (!empty(json_encode($history_kategori))); ?>){
				var id_kategori = <?php echo json_encode($history_kategori); ?>;
				var id_sub_kategori = <?php echo (!empty($_GET['subkategori']) ? $_GET['subkategori'] : 0)?>;
	    		if (id_kategori) {
					var id=id_kategori;
		            $.ajax({
		                url : "<?php echo base_url();?>/ApiOpsi/subkategori",
		                method : "POST",
		                data : {id: id},
		                async : false,
		                dataType : 'json',
		                success: function(data){
							var html = '';
		                    var i;
		                    var selected = "";
		                    //alert(data.results.cities.length);
		                    html += "<option value=''>- Pilih Sub Kategori -</option>";
		                    for(i=0; i<data.results.travel_categories.length; i++){		                    	
		                    	if(id_sub_kategori==data.results.travel_categories[i].id){
		                    		html += '<option selected value='+data.results.travel_categories[i].id+'>'+data.results.travel_categories[i].sub_travel_category_name+'</option>';
		                    	} else{
			                        html += '<option value='+data.results.travel_categories[i].id+'>'
			                        +data.results.travel_categories[i].sub_travel_category_name+'</option>';
			                    }
		                    }
		                    $('.subkategori').html(html);

		                }
		            });
	    		}
	    	}


	    	if(<?php echo json_encode($history_provinsi); ?> != null){
		    	var id_prov  = <?php echo json_encode($history_provinsi); ?>;
		    	var id_kota  = <?php echo json_encode($history_kota); ?>;
		    	var id_kec   = <?php echo json_encode($history_kecamatan); ?>;
		    	var id_objek   = <?php echo json_encode($history_traveling_id); ?>;

		    	if (id_prov) {
		    		var id=id_prov;
		            $.ajax({
		                url : "<?php echo base_url();?>/ApiOpsi/kotakabupaten",
		                method : "POST",
		                data : {id: id},
		                async : false,
		                dataType : 'json',
		                success: function(data){
							var html = '';
		                    var i;
		                    var selected = "";
		                    //alert(data.results.cities.length);
		                    html += "<option value=''>Pilih Kab/Kota</option>";
		                    for(i=0; i<data.results.cities.length; i++){
		                    	if(id_kota==data.results.cities[i].city_id){
		                    		html += '<option selected value='+data.results.cities[i].city_id+'>'+data.results.cities[i].city_name+'</option>';
		                    	} else{
		                    		html += '<option value='+data.results.cities[i].city_id+'>'+data.results.cities[i].city_name+'</option>';
		                    	}
		                    }
		                    $('.kotakabupaten').html(html); 

		                     $.ajax({
				                url : "<?php echo base_url();?>ApiOpsi/travels_list",
				                method : "POST",
				                data : {id: id_kota},
				                async : false,
				                dataType : 'json',
				                success: function(data){
									var html = '';
				                    var i;
				                    html += "<option value=''>- Pilih Objek Wisata -</option>";
				                    for(i=0; i<data.results.travels.length; i++){
				                    	if(id_objek==data.results.travels[i].id){
				                    		html += '<option selected value='+data.results.travels[i].id+'>'+data.results.travels[i].travel_name+'</option>';
				                    	} else{
				                    		
				                        	html += '<option value='+data.results.travels[i].id+'>'+data.results.travels[i].travel_name+'</option>';
				                    	}

				                    }
				                    $('.travel').html(html);
				                }
				            });

		                    /*$.ajax({
				                url : "<?php echo base_url();?>/page/kecamatan",
				                method : "POST",
				                data : {id: id_kota},
				                async : false,
				                dataType : 'json',
				                success: function(data){
									var html = '';
				                    var i;
				                    html += "<option value=''>Pilih Kecamatan</option>";
				                    for(i=0; i<data.results.villages.length; i++){
				                    	if(id_kec==data.results.villages[i].village_id){
				                    		html += '<option selected value='+data.results.villages[i].village_id+'>'+data.results.villages[i].village_name+'</option>';
				                    	} else{
				                    		
				                        	html += '<option value='+data.results.villages[i].village_id+'>'+data.results.villages[i].village_name+'</option>';
				                    	}

				                    }
				                    $('.kecamatan').html(html);
				                }
				            });*/

		                }
		            });
		    	}

    		}

    	$('#kategori').change(function(){
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url();?>/ApiOpsi/Subkategori",
                method : "POST",
                data : {id: id},
                async : false,
                dataType : 'json',
                success: function(data){
					var html = '';
                    var i;
                    //alert(data.results.cities.length);
                    html += "<option value=''>- Pilih Sub Kategori -</option>";
                    for(i=0; i<data.results.travel_categories.length; i++){
                        html += '<option value='+data.results.travel_categories[i].id+'>'
                        +data.results.travel_categories[i].sub_travel_category_name+'</option>';
                    }
                    $('.subkategori').html(html);

                    
                     
                }
            });
        });



        $('#provinsi').change(function(){
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url();?>/ApiOpsi/kotakabupaten",
                method : "POST",
                data : {id: id},
                async : false,
                dataType : 'json',
                success: function(data){
					var html = '';
                    var i;
                    //alert(data.results.cities.length);
                    html += "<option value=''>Pilih Kab/Kota</option>";
                    if(data.results !== null){
	                    for(i=0; i<data.results.cities.length; i++){
	                    	//alert("aaaa " + data.results.cities[i].city_name);
	                        html += '<option value='+data.results.cities[i].city_id+'>'+data.results.cities[i].city_name+'</option>';
	                    }
	                    $('.kotakabupaten').html(html);
	                } else{
	                	 html = '<option value=0>Pilih Kab / Kota</option>';

	                     $('.kotakabupaten').html(html);

	                }

                    
                     
                }
            });
        });

        $('#kotakabupaten').change(function(){
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url();?>/ApiOpsi/kecamatan",
                method : "POST",
                data : {id: id},
                async : false,
                dataType : 'json',
                success: function(data){
					var html = '';
                    var i;
                    //alert(data.results.cities.length);
                    html += "<option value=''>Pilih Kecamatan</option>";
                    for(i=0; i<data.results.villages.length; i++){
                    	//alert("aaaa " + data.results.cities[i].city_name);
                        html += '<option value='+data.results.villages[i].village_id+'>'+data.results.villages[i].village_name+'</option>';
                    }
                    $('.kecamatan').html(html);

                    
                     
                }
            });
        });

        $('#kotakabupaten_travels').change(function(){
            var city_id=$(this).val();
            $.ajax({
                url : "<?php echo base_url();?>ApiOpsi/travels_list",
                method : "GET",
                data : {"city_id" : city_id},
                async : false,
                dataType : 'json',
                success: function(data){                	
					var html = '';
                    var i;
                    //alert(data.results.cities.length);
                    html += "<option value=''>Pilih Objek</option>";
                    if(data.results != null && data.results.travels != null){
	                    for(i=0; i<data.results.travels.length; i++){
	                    	//alert("aaaa " + data.results.cities[i].city_name);
	                        html += '<option value='+data.results.travels[i].id+'>'+data.results.travels[i].travel_name+'</option>';
	                    }
	                }
                    $('.travel').html(html);

                    
                     
                }
            });
        });
        $(".point_name").html($("#point").find(":selected").text()+" "+($("#kategori").find(":selected").text() == "- Pilih Kategori -" ? "" : $("#kategori").find(":selected").text())+" "+($(".subkategori_data").find(":selected").text() == "- Pilih Sub Kategori -" ? "" : $(".subkategori_data").find(":selected").text())+" "+($("#provinsi").find(":selected").text() == "- Pilih Provinsi -" ? "Nusantara" : $("#provinsi").find(":selected").text()));
    });
</script>


<script>

function previewImage(id) {
	// Get the modal
	var modal = document.getElementById("previewImage"+id);

	// Get the image and insert it inside the modal - use its "alt" text as a caption
	var img = document.getElementById("myImg"+id);
	var modalImg = document.getElementById("img"+id);
	var captionText = document.getElementById("caption");
	modal.style.display = "block";
	modalImg.src = img.src;
	//captionText.innerHTML = this.alt;
	
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close"+id)[0];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() { 
	  modal.style.display = "none";
	}
}
</script>


</html>