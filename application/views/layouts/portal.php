<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Masuk - Komite Pengerak Pariwisata Jawa Barat</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Akademi Fisioterapi Rumah Sakit Dustira">
		<meta name="author" content="Sampulkreativ Technology">
		<link href="<?=base_url();?>assets/stylesheet/bootstrap.min.css" rel="stylesheet">
		<link href="<?=base_url();?>assets/stylesheet/login.css" rel="stylesheet">
		<link href="<?=base_url();?>assets/stylesheet/style.css" rel="stylesheet">
		<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
		<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
	</head>
	<body>
		<?php if($middle) echo $middle; ?>
	</body>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/form-validation.js"></script>
</html>