<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Akademi Fisioterapi Rumah Sakit Dustira">
	<meta name="author" content="Sampulkreativ Technology">
	<title><?php echo $title ?></title>
	<base href="http://www.cyclistinsuranceaustralia.com.au/">
	<link href="<?=base_url();?>assets/stylesheet/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/stylesheet/core.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/stylesheet/components.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/stylesheet/icons.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/stylesheet/pages.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/stylesheet/menu.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/stylesheet/responsive.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/stylesheet/datatables.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/stylesheet/style.css" rel="stylesheet">
	<script src="<?=base_url();?>assets/js/modernizr.min.js"></script>
	<link href="<?=base_url();?>assets/stylesheet/bootstrap-datepicker.min.css" rel="stylesheet">
	<style type="text/css">
		table.dataTable thead th.sorting::after{
			content: none !important;
		}
		table.dataTable thead th.sorting_asc::after{
			display: none !important;
		}
	</style>
</head>
<body>
	<header id="topnav">
		<?php $this->load->view('shared/header'); ?>
		<?php $this->load->view('shared/menu'); ?>
	</header>
	<div class="wrapper">
			<div class="container">
				<?php $this->load->view('shared/breadcum'); ?>
				<?php if($middle) echo $middle; ?>
			</div>
		</div>
	<?php $this->load->view('shared/footer'); ?>

	<script src="<?=base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?=base_url();?>assets/js/detect.js"></script>
	<script src="<?=base_url();?>assets/js/fastclick.js"></script>

	<script src="<?=base_url();?>assets/js/jquery.slimscroll.js"></script>
	<script src="<?=base_url();?>assets/js/jquery.blockUI.js"></script>
	<script src="<?=base_url();?>assets/js/waves.js"></script>
	<script src="<?=base_url();?>assets/js/wow.min.js"></script>
	<script src="<?=base_url();?>assets/js/jquery.nicescroll.js"></script>
	<script src="<?=base_url();?>assets/js/jquery.scrollTo.min.js"></script>
	<script src="<?=base_url();?>assets/js/bootstrap-datepicker.js"></script>
	<script src="<?=base_url();?>assets/js/global.js"></script>

	<script src="<?=base_url();?>assets/js/jquery.core.js"></script>
	<script src="<?=base_url();?>assets/js/jquery.app.js"></script>
	<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/form-validation.js"></script>
</body>
</html>
<script type="text/javascript">
	$('#datatable').DataTable();
	$("#datatable_length").remove();
	$("#datatable_filter").remove();
</script>