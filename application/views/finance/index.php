<style type="text/css">
	.upload_bukti{
		border: 1px solid #000;
		padding: 5px;
	}
</style>

<div class="row">
	<div class="col-lg-12">
		<?php $this->load->view('shared/errors'); ?>
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#home" data-toggle="tab" aria-expanded="true">
					<span class="visible-xs"><i class="fa fa-home"></i></span>
					<span class="hidden-xs">Transaksi Pembayaran</span>
				</a>
			</li>
			<li class="">
				<a href="#messages" data-toggle="tab" aria-expanded="false">
					<span class="visible-xs"><i class="fa fa-money"></i></span>
					<span class="hidden-xs">Uang Biaya Kuliah</span>
				</a>
			</li>
			<li class="">
				<a href="#confirmation" data-toggle="tab" aria-expanded="false">
					<span class="visible-xs"><i class="fa fa-file-o"></i></span>
					<span class="hidden-xs">Konfirmasi Pembayaran</span>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="home">
				<h3>Transaksi Pembayaran</h3>
				<table id="tech-companies-1" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="3%">No</th>
							<th data-priority="1" width="15%">Tanggal Bayar</th>
							<th data-priority="3" width="15%">Jumlah</th>
							<th data-priority="1" width="20%">Petugas</th>
							<th data-priority="3" width="25%">Keterangan</th>
						</tr>
					</thead>
					<tbody>
						<?php if($result_data->results != null){ ?>
							<?php $no = 1;$index=0;foreach ($result_data->results as $finance) { ?>
								<?php $index ?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $finance[$index]->waktu ?></td>
									<td>Rp.<?php echo number_format($finance[$index]->jumlah) ?></td>
									<td><?php echo $finance[$index]->petugas ?></td>
									<td><?php echo $finance[$index]->keterangan ?></td>
								</tr>
							<?php } ?>
						<?php }else{ ?>
							<tr><th colspan="6"><center><?php echo $result_data->messages ?></center></th></tr>
						<?php } ?>
					 </tbody>
				</table>
			</div>
			<div class="tab-pane" id="messages">
				<h3>Uang Biaya Kuliah</h3>
				<table id="tech-companies-1" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th data-priority="1">Keterangan</th>
							<th data-priority="3">Jumlah</th>
							<th data-priority="1">Sisa</th>
							<th data-priority="3">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php if($result_data_pembayaran->results != null){ ?>
							<?php $no = 1;$index=0;foreach ($result_data_pembayaran->results as $finance) { ?>
								<?php $index ?>
								<tr>
									<td><?php echo $no ?></td>
									<td><?php echo $finance[$index]->keterangan ?></td>
									<td>Rp.<?php echo number_format($finance[$index]->jumlah) ?></td>
									<td>Rp.<?php echo number_format($finance[$index]->sisa) ?></td>
									<td><?php echo $finance[$index]->status ?></td>
								</tr>
							<?php } ?>
						<?php }else{ ?>
							<tr><th colspan="7"><center><?php echo $result_data_pembayaran->messages ?></center></th></tr>
						<?php } ?>
					 </tbody>
				</table>
			</div>
			<div class="tab-pane" id="confirmation">
				<h3>Konfirmasi Pembayaran</h3>
				<form action="<?php echo base_url() ?>finance/create" method="post" enctype="multipart/form-data">
				<center>
					<h4>Upload bukti pembayaran Anda</h4>
					<input type="file" name="bukti" class="form-control data-input" style="width:300px" data-required="true">
					<br>
					<button class="btn btn-primary">Kirim</button>
				</center>
			</div>
		</div>
	</div>
</div>