<!-- <span class="pull-right">
	<a href="<?php echo base_url() ?>study?download=pdf&tab=transkrip" class="btn btn-danger">Download PDF</a>
</span>
<br> -->
<h3>IPK : <?php echo $result_data_transkrip->results->transkip->ipk ?></h3>
<table id="tech-companies-1" class="table table-striped table-bordered table-hover">
	<?php $no = 1; foreach ($result_data_transkrip->results->transkip->data as $data) { ?>
		<thead>
			<tr>
				<th colspan="5">Semester <?php echo $data->semester ?></th>
			</tr>
			<tr>
				<th width="5%">No</th>
				<th data-priority="3">Mata Kuliah</th>
				<th data-priority="1">Nilai Bobot</th>
			</tr>
		</thead>
		<tbody>
			<?php if($data->nilai != null){ ?>
				<?php $nomor = 1; foreach ($data->nilai as $nilai) { ?>
					<tr>
						<td><?php echo $nomor++ ?></td>
						<td><?php echo $nilai->matakuliah->nama ?></td>
						<td><?php echo $nilai->nilai_bobot ?></td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<th colspan="3"><center>Tidak ada data</center></th>
				</tr>
			<?php } ?>
		</tbody>
	<?php } ?>
</table>