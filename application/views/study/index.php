<div class="row">
	<div class="col-lg-12">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#home" data-toggle="tab" aria-expanded="true">
					<span class="visible-xs"><i class="fa fa-sort-numeric-asc"></i></span>
					<span class="hidden-xs">Nilai Per Semester</span>
				</a>
			</li>
			<li class="">
				<a href="#profile" data-toggle="tab" aria-expanded="false">
					<span class="visible-xs"><i class="fa fa-sort-alpha-asc"></i></span>
					<span class="hidden-xs">Nilai Transkrip</span>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="home">
				<h3>Nilai Per Semester</h3>
				<span class="pull-right">
					<a href="<?php echo base_url() ?>study?download=pdf&tab=home&semester=<?php echo isset($_GET['semester']) ? $_GET['semester'] : '' ?>" class="btn btn-danger">Download PDF</a>
				</span>
				<div class="col-lg-3" style="padding-left:0px">
					<form class="form-horizontal" action="<?=base_url();?>study/index" method="get">
						<div class="form-group input-group" style="margin:0">
							<select name="semester" class="selectpicker form-control data-input" data-required="true">
								<option value="">-- Pilih Semester --</option>
								<option value="1" <?php echo isset($_GET['semester']) && $_GET['semester'] == '1' ? 'selected' : '' ?>>Semester 1</option>
								<option value="2" <?php echo isset($_GET['semester']) && $_GET['semester'] == '2' ? 'selected' : '' ?>>Semester 2</option>
								<option value="3" <?php echo isset($_GET['semester']) && $_GET['semester'] == '3' ? 'selected' : '' ?>>Semester 3</option>
								<option value="4" <?php echo isset($_GET['semester']) && $_GET['semester'] == '4' ? 'selected' : '' ?>>Semester 4</option>
								<option value="5" <?php echo isset($_GET['semester']) && $_GET['semester'] == '5' ? 'selected' : '' ?>>Semester 5</option>
								<option value="6" <?php echo isset($_GET['semester']) && $_GET['semester'] == '6' ? 'selected' : '' ?>>Semester 6</option>
							</select>
							<span class="input-group-btn">
								<?php echo form_submit(array('class' => 'btn btn-warning', 'value' => 'Cari')); ?>
							</span>
						</div>
					</form>
				</div>
				<div class="clearfix"></div>
				<br>
				<table id="tech-companies-1" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th data-priority="3">Mata Kuliah</th>
							<th data-priority="1">Nilai tugas 1</th>
							<th data-priority="1">Nilai tugas 2</th>
							<th data-priority="1">Nilai tugas 3</th>
							<th data-priority="1">Nilai tugas 4</th>
							<th data-priority="1">UTS</th>
							<th data-priority="3">UAS</th>
							<th data-priority="1">Nilai akhir</th>
							<th data-priority="1">Nilai bobot</th>
						</tr>
					</thead>
					<tbody>
						<?php if($result_data->results != null){ ?>
							<?php $no = 1;foreach ($result_data->results->nilai as $data) { ?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $data->matakuliah->nama ?></td>
									<td><?php echo $data->nilai_tugas1 ?></td>
									<td><?php echo $data->nilai_tugas2 ?></td>
									<td><?php echo $data->nilai_tugas3 ?></td>
									<td><?php echo $data->nilai_tugas4 ?></td>
									<td><?php echo $data->nilai_uts ?></td>
									<td><?php echo $data->nilai_uas ?></td>
									<td><?php echo $data->nilai_akhir ?></td>
									<td><?php echo $data->nilai_bobot ?></td>
								</tr>
							<?php } ?>
						<?php } else { ?>
							<tr>
								<th colspan="10"><center><?php echo $result_data->messages ?></center></th>
							</tr>
						<?php } ?>
					 </tbody>
				</table>
			</div>
			<div class="tab-pane" id="profile">
				<h3>Nilai Transkrip</h3>
				<?php $this->load->view('study/shared/transkrip'); ?>
			</div>
			<!-- <div class="tab-pane" id="messages">
				<h3></h3>
			</div> -->
		</div>
	</div>
</div>