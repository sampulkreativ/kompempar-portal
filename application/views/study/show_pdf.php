<div style="position: absolute;"><img src="<?= base_url(); ?>assets/images/logo.png" width="90px"></div>
<div style="text-align: center">
	<h3>Informasi Hasil Belajar Mahasiswa</h3>
	<h4>Nilai Semester <?php echo isset($_GET['semester']) ?></h4>
	<h4 style="margin-top: -10px; font-weight: normal">(<?= $this->session->userdata('nim') ?>) <?= $this->session->userdata('nama') ?></h4>
</div>
<table border="1" style=" border-collapse: collapse;">
	<thead>
		<tr>
			<th style="padding: 5px" width="100%">No</th>
			<th style="padding: 5px" width="100%" data-priority="3">Mata Kuliah</th>
			<th style="padding: 5px" width="100%" data-priority="1">Nilai tugas 1</th>
			<th style="padding: 5px" width="100%" data-priority="1">Nilai tugas 2</th>
			<th style="padding: 5px" width="100%" data-priority="1">Nilai tugas 3</th>
			<th style="padding: 5px" width="100%" data-priority="1">Nilai tugas 4</th>
			<th style="padding: 5px" width="100%" data-priority="1">UTS</th>
			<th style="padding: 5px" width="100%" data-priority="3">UAS</th>
			<th style="padding: 5px" width="100%" data-priority="1">Nilai akhir</th>
			<th style="padding: 5px" width="100%" data-priority="1">Nilai bobot</th>
		</tr>
	</thead>
	<tbody>
		<?php if($result_data->results != null){ ?>
			<?php $no = 1;foreach ($result_data->results->nilai as $data) { ?>
				<tr>
					<td style="padding: 5px" width="100%"><?php echo $no++ ?></td>
					<td style="padding: 5px" width="100%"><?php echo $data->matakuliah->nama ?></td>
					<td style="padding: 5px" width="100%"><?php echo $data->nilai_tugas1 ?></td>
					<td style="padding: 5px" width="100%"><?php echo $data->nilai_tugas2 ?></td>
					<td style="padding: 5px" width="100%"><?php echo $data->nilai_tugas3 ?></td>
					<td style="padding: 5px" width="100%"><?php echo $data->nilai_tugas4 ?></td>
					<td style="padding: 5px" width="100%"><?php echo $data->nilai_uts ?></td>
					<td style="padding: 5px" width="100%"><?php echo $data->nilai_uas ?></td>
					<td style="padding: 5px" width="100%"><?php echo $data->nilai_akhir ?></td>
					<td style="padding: 5px" width="100%"><?php echo $data->nilai_bobot ?></td>
				</tr>
			<?php } ?>
		<?php } else { ?>
			<tr>
				<th style="padding: 5px" width="100%" colspan="10"><center><?php echo $result_data->messages ?></center></th>
			</tr>
		<?php } ?>
	 </tbody>
</table>