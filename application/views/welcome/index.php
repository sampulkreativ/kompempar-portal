<div class="row">
	<div class="col-lg-7">
		<div class="card-box center-responsive">
			<div class="col-md-1">
				<img class="md" src="<?=base_url();?>assets/images/finance2.png">
			</div>
			<div class="col-md-8">
				<h4 class="text-dark header-title m-t-0"><a href="<?=base_url();?>finance">Keuangan</a></h4>
				<p>Riwayat pembayaran, transaksi, unggah bukti pembayaran</p>
			</div>
			<div class="clearfix"></div>
			<br>
			<div class="col-md-1">
				<img class="md" src="<?=base_url();?>assets/images/study2.png">
			</div>
			<div class="col-md-8">
				<h4 class="text-dark header-title m-t-0"><a href="<?=base_url();?>study">Hasil Belajar</a></h4>
				<p>Laporan nilai per semester, transkrip.</p>
			</div>
			<div class="clearfix"></div>
			<br>
			<div class="col-md-1">
				<img class="md" src="<?=base_url();?>assets/images/schedule2.png">
			</div>
			<div class="col-md-8">
				<h4 class="text-dark header-title m-t-0"><a href="<?=base_url();?>schedule">Jadwal</a></h4>
				<p>Jadwal kuliah & praktikum persemester, UTS/UAS, materi/tugas.</p>
			</div>			
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="col-lg-5">
		<div class="card-box">
			<div class="col-lg-6">
				<img class="md" src="<?php echo $result_data->profil->foto == '' ? base_url().'/assets/images/missing.jpg' : $result_data->profil->foto ?>" width="100%" style="margin-left: -10px">
			</div>
			<div class="col-lg-6" style="margin-left: -10px">
				<h4 class="text-dark header-title m-t-0">Program Belajar</h4>
				<p><?php echo $result_data->profil->fakultas; echo" ". $result_data->profil->jurusan?> </p>
				<h4 class="text-dark header-title m-t-0">Email</h4>
				<p><?php echo $result_data->profil->email ?></p>
				<h4 class="text-dark header-title m-t-0">No. Telp</h4>
				<p><?php echo $result_data->profil->no_telp ?></p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>