<style type="text/css">
	select {
  -webkit-appearance: block;
  -moz-appearance: block;
  appearance: block;
}
</style>
<div class="row">
	<div class="col-lg-12">
		<?php $this->load->view('shared/errors'); ?>
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#akun" data-toggle="tab" aria-expanded="true">
					<span class="visible-xs"><i class="fa fa-user"></i></span>
					<span class="hidden-xs">Pengaturan Akun</span>
				</a>
			</li>
			<li class="">
				<a href="#profile" data-toggle="tab" aria-expanded="false">
					<span class="visible-xs"><i class="fa fa-gear"></i></span>
					<span class="hidden-xs">Pengaturan Biodata</span>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="akun">
				<h3>Pengaturan Akun</h3>
				<form class="form-horizontal" action="<?=base_url()?>setting/update_password" method="post">
					<div class="control-group">
						<div class="control-label col-lg-2">
							<lable class="pull-left">Sandi Lama</lable>
						</div>
					</div>
					<div class="controls col-lg-4">
						<input type="password" name="password_lama" class="form-control data-input" data-required="true">
					</div>
					<div class="clearfix clearfix-profile"></div>
					<div class="control-group">
						<div class="control-label col-lg-2">
							<lable class="pull-left">Sandi Baru</lable>
						</div>
					</div>
					<div class="controls col-lg-4">
						<input type="password" name="password" class="form-control data-input" data-required="true">
					</div>
					<div class="clearfix clearfix-profile"></div>
					<div class="control-group">
						<div class="control-label col-lg-2">
							<lable class="pull-left">Konfirmasi Sandi</lable>
						</div>
					</div>
					<div class="controls col-lg-4">
						<input type="password" name="conf_password" class="form-control data-input" data-required="true">
					</div>
					<div class="clearfix clearfix-profile"></div>
					<hr>
					<br>
					<div class="controls col-lg-2">
						<button class="btn btn-primary btn-block">Perbaharui</button>
					</div>
				</form>
			</div>
			<div class="tab-pane" id="profile">
				<h3>Pengaturan Biodata</h3>
				<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?=base_url()?>setting/update_profile">
					<div class="col-lg-3">
						<img class="md" src="<?php echo $result_data->profil->foto == '' ? base_url().'/assets/images/missing.jpg' : $result_data->profil->foto ?>" width="100%">
						<div class="clearfix clearfix-profile"></div>
						<input type="file" name="photo" class="form-control">
					</div>
					<div class="col-lg-9">
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">Email</lable>
							</div>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="email" class="form-control data-input" placeholder="email@email.co.id" value="<?php echo isset($result_data->profil->email) ? $result_data->profil->email : '' ?>" data-required="true">
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">NIM</lable>
							</div>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="nim" class="form-control data-input" disabled="disabled" value="<?php echo isset($result_data->profil->nim) ? $result_data->profil->nim : '' ?>">
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">NIK</lable>
							</div>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="nik" class="form-control data-input" value="<?php echo isset($result_data->profil->nik) ? $result_data->profil->nik : '' ?>">
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">Fakultas / Prodi</lable>
							</div>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="" class="form-control data-input" disabled="disabled" value="<?php echo $result_data->profil->fakultas; echo" - ". $result_data->profil->jurusan?>">
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">Nama Lengkap</lable>
							</div>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="" class="form-control data-input" value="<?php echo isset($result_data->profil->nama) ? $result_data->profil->nama : '' ?>" data-required="true" disabled="disabled">
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-label col-lg-2">
							<lable class="pull-left">Semester</lable>
						</div>
						<div class="controls col-lg-1">
							<input type="text" name="semester" class="form-control data-input" disabled="disabled" value="<?php echo isset($result_data->profil->semester) ? $result_data->profil->semester : '' ?>">
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">Tahun Masuk</lable>
							</div>
						</div>
						<div class="controls col-lg-2">
							<input type="text" name="tahun_masuk" class="form-control data-input" disabled="disabled" value="<?php echo isset($result_data->profil->tahun_masuk) ? $result_data->profil->tahun_masuk : '' ?>">
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-label col-lg-2">
							<lable class="pull-left">Dosen wali</lable>
						</div>
						<div class="controls col-lg-3">
							<input type="text" name="dosen_wali" class="form-control data-input" disabled="disabled" value="<?php echo isset($result_data->profil->dosen_wali) ? $result_data->profil->dosen_wali : '' ?>">
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">Tmpt/Tgl Lahir</lable>
							</div>
						</div>
						<div class="controls col-lg-4">
							<input type="text" name="tempat_lahir" class="form-control data-input" value="<?php echo isset($result_data->profil->tempat_lahir) ? $result_data->profil->tempat_lahir : '' ?>" data-required="true">
						</div>
						<div class="clearfix-profile"></div>
						<div class="controls col-lg-2">
							<select name="tanggal" class="form-control" data-required="true">
								<?php for($i=1; $i<=31;$i++) { ?>
									<option value="<?php echo $i ?>" <?php echo substr($result_data->profil->tanggal_lahir, 8, 2) == $i ? 'selected' : '' ?>><?php echo $i ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="clearfix-profile"></div>
						<div class="controls col-lg-2">
							<select name="bulan" class="form-control" data-required="true">
								<option value="01" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "01" ? 'selected' : '' ?>>Januari</option>
								<option value="02" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "02" ? 'selected' : '' ?>>Februari</option>
								<option value="03" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "03" ? 'selected' : '' ?>>Maret</option>
								<option value="04" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "04" ? 'selected' : '' ?>>April</option>
								<option value="05" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "05" ? 'selected' : '' ?>>Mei</option>
								<option value="06" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "06" ? 'selected' : '' ?>>Juni</option>
								<option value="07" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "07" ? 'selected' : '' ?>>Juli</option>
								<option value="08" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "08" ? 'selected' : '' ?>>Agustus</option>
								<option value="09" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "09" ? 'selected' : '' ?>>September</option>
								<option value="10" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "10" ? 'selected' : '' ?>>Oktober</option>
								<option value="11" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "11" ? 'selected' : '' ?>>NOvember</option>
								<option value="12" <?php echo substr($result_data->profil->tanggal_lahir, 5, 2) == "12" ? 'selected' : '' ?>>Desember</option>
							</select>
						</div>
						<div class="clearfix-profile"></div>
						<div class="controls col-lg-2">
							<select name="tahun" class="form-control data-input" data-required="true">
								<?php for($i=1991; $i<=2017;$i++) { ?>
									<option value="<?php echo $i ?>" <?php echo substr($result_data->profil->tanggal_lahir, 0, 4) == $i ? 'selected' : '' ?>><?php echo $i ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">Jenis Kelamin</lable>
							</div>
						</div>
						<div class="controls col-lg-5">
							<select name="jenis_kelamin" class="form-control data-input" data-required="true">
								<option value="L" <?php echo isset($result_data->profil->jenis_kelamin->kode) && $result_data->profil->jenis_kelamin->kode == 'L' ? 'selected' : '' ?>>Laki-laki</option>
								<option value="P" <?php echo isset($result_data->profil->jenis_kelamin->kode) && $result_data->profil->jenis_kelamin->kode == 'P' ? 'selected' : '' ?>>Perempuan</option>
							</select>
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">Agama</lable>
							</div>
						</div>
						<div class="controls col-lg-5">
							<select name="agama" class="form-control data-input" data-required="true">
								<option value="Islam" <?php echo isset($result_data->profil->agama) && $result_data->profil->agama == 'Islam' ? 'selected' : '' ?>>Islam</option>
								<option value="Kristen" <?php echo isset($result_data->profil->agama) && $result_data->profil->agama == 'Kristen' ? 'selected' : '' ?>>Kristen</option>
								<option value="Hindu" <?php echo isset($result_data->profil->agama) && $result_data->profil->agama == 'Hindu' ? 'selected' : '' ?>>Hindu</option>
								<option value="Budha" <?php echo isset($result_data->profil->agama) && $result_data->profil->agama == 'Budha' ? 'selected' : '' ?>>Budha</option>
								<option value="Kepercayaan" <?php echo isset($result_data->profil->agama) && $result_data->profil->agama == 'Kepercayaan' ? 'selected' : '' ?>>Kepercayaan</option>
							</select>
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">Alamat</lable>
							</div>
						</div>
						<div class="controls col-lg-6">
							<textarea class="form-control data-input" rows="2" data-required="true"><?php echo isset($result_data->profil->alamat) ? $result_data->profil->alamat : '' ?></textarea>
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">Propinsi - Kota</lable>
							</div>
						</div>
						<div class="controls col-lg-4">
							<select name="propinsi" class="form-control data-input" data-required="true">
								<?php foreach ($result_provinces->provinsi as $province){ ?>
									<option value="<?php echo $province->id ?>" <?php echo $province->id == $result_data->profil->provinsi->id ? "selected" : "" ?>><?php echo $province->nama ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="clearfix-profile"></div>
						<div class="controls col-lg-6">
							<select name="kota" class="form-control data-input" data-required="true">
								<?php foreach ($result_cities->kota as $city){ ?>
									<option value="<?php echo $city->id ?>" <?php echo $city->id == $result_data->profil->kota->id ? "selected" : "" ?>><?php echo $city->nama ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="clearfix clearfix-profile"></div>
						<div class="control-group">
							<div class="control-label col-lg-2">
								<lable class="pull-left">No. Telepon</lable>
							</div>
						</div>
						<div class="controls col-lg-5">
							<input type="text" name="no_telp" class="form-control data-input" value="<?php echo isset($result_data->profil->no_telp) ? $result_data->profil->no_telp : '' ?>" data-required="true">
						</div>
					</div>					
					<div class="clearfix"></div>
					<hr><br>
					<div class="controls col-lg-2">
						<button class="btn btn-primary btn-block">Perbaharui</button>
					</div>
				</form>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>