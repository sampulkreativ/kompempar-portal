<div style="position: absolute;"><img src="<?= base_url(); ?>assets/images/logo.png" width="90px"></div>
<div style="text-align: center">
	<h3>Jadwal <?php if($_GET['schedule'] == 'tugas'){
					echo 'Materi/Tugas Kuliah';
				}elseif($_GET['schedule'] == 'kuliah'){
					echo 'Kuliah dan Praktikum';
					}else{
						echo 'UTS/UAS';
						} ?></h3>
	<h4 style="margin-top: -10px; font-weight: normal">(<?= $this->session->userdata('nim') ?>) <?= $this->session->userdata('nama') ?></h4>
</div>
<br><br>
<?php if($_GET['schedule'] == 'tugas'){ ?>
	<table border="1" style=" border-collapse: collapse;">
		<thead>
			<tr>
				<th style="padding: 5px" width="10%">No</th>
				<th style="padding: 5px" width="100%">Mata Kuliah</th>
				<th style="padding: 5px" width="100%">Tanggal</th>
				<th style="padding: 5px" width="100%">Tipe</th>
				<th style="padding: 5px" width="100%">Deskripsi</th>
			</tr>
		</thead>
		<tbody>
			<?php if($result_data_tugas->status == 'ERROR'){ ?>
				<tr>
					<th style="padding: 5px" colspan="5"><center><?php echo $result_data_tugas->messages ?></center></th>
				</tr>
			<?php }else{ ?>
				<?php $no=1; foreach ($result_data_tugas->results->materi as $schedule){?>
					<tr>
						<td><?php echo $no++ ?>.</td>
						<td><?php echo $schedule->matakuliah ?></td>
						<td><?php echo $schedule->tanggal ?></td>
						<td><?php echo $schedule->materi->tipe ?></td>
						<td><?php echo $schedule->materi->deskripsi == "" ? "-" : $schedule->materi->deskripsi ?></td>
					</tr>
				<?php } ?>
			<?php } ?>
		 </tbody>
	</table>
<?php }elseif($_GET['schedule'] == 'kuliah'){ ?>
	<table border="1" style=" border-collapse: collapse;" width="100%">
		<thead>
			<tr>
				<th>No</th>
				<th style="padding: 5px" data-priority="3">Mata Kuliah</th>
				<th style="padding: 5px" data-priority="1">SKS</th>
				<th style="padding: 5px" data-priority="3">Dosen</th>
				<th style="padding: 5px" data-priority="3">Kls</th>
				<th style="padding: 5px" data-priority="3">Ruang/Gedung</th>
				<th style="padding: 5px" data-priority="3">Hari</th>
				<th style="padding: 5px" data-priority="3">Jam</th>
				<th style="padding: 5px" data-priority="3">Tahun Ajaran</th>
				<th style="padding: 5px">Silabus</th>
			</tr>
		</thead>
		<tbody>
			<?php if($result_data->status == 'ERROR'){ ?>
				<tr>
					<th style="padding: 5px" colspan="10"><center><?php echo $result_data->messages ?></center></th>
				</tr>
			<?php }else{ ?>
				<?php $no=1; foreach ($result_data->results->jadwal as $schedule){?>
					<tr>
						<td><?php echo $no++ ?>.</td>
						<td><?php echo $schedule->matakuliah ?></td>
						<td><?php echo $schedule->sks ?></td>
						<td><?php echo $schedule->dosen ?></td>
						<td><?php echo $schedule->kelas ?></td>
						<td><?php echo $schedule->ruangan ?>/<?php echo $schedule->gedung ?></td>
						<td><?php echo $schedule->hari ?></td>
						<td><?php echo $schedule->jam_mulai ?> s/d <?php echo $schedule->jam_akhir ?></td>
						<td><?php echo $schedule->tahun_ajaran ?></td>
						<td><?php echo $schedule->silabus == "" ? "<span style='color: red'>Kosong</span>" : "<a href=".$schedule->silabus." class='btn btn-white'><i class='fa fa-download></i></a>'" ?></td>
					</tr>
				<?php } ?>
			<?php } ?>
		 </tbody>
	</table>
<?php }else{ ?>
	<table border="1" style=" border-collapse: collapse;" width="100%">
		<thead>
			<tr>
				<th>No</th>
				<th style="padding: 5px" data-priority="3">Mata Kuliah</th>
				<th style="padding: 5px" data-priority="1">Tanggal</th>
				<th style="padding: 5px" data-priority="3">Jam</th>
				<th style="padding: 5px" data-priority="3">Ruang/Gedung</th>
				<th style="padding: 5px" data-priority="3">Tahun Ajaran</th>
			</tr>
		</thead>
		<tbody>
			<?php if($result_data_uas->status == 'ERROR'){ ?>
				<tr>
					<th style="padding: 5px" colspan="6"><center><?php echo $result_data_uas->messages ?></center></th>
				</tr>
			<?php }else{ ?>
				<?php $no=1; foreach ($result_data_uas->results->jadwal as $schedule){?>
					<tr>
						<td><?php echo $no++ ?>.</td>
						<td><?php echo $schedule->matakuliah ?></td>
						<td><?php echo $schedule->tanggal ?></td>
						<td><?php echo $schedule->jam_mulai ?> s/d <?php echo $schedule->jam_akhir ?></td>
						<td><?php echo $schedule->ruangan ?>/<?php echo $schedule->gedung ?></td>				
						<td><?php echo $schedule->tahun_ajaran ?></td>
					</tr>
				<?php } ?>
			<?php } ?>
		 </tbody>
	</table>
<?php } ?>