<div class="row">
	<div class="col-lg-12">
		<ul class="nav nav-tabs">
			<li class="<?php echo isset($tugas) ? $tugas : "" ?>">
				<a href="#tugas" data-toggle="tab" aria-expanded="false">
					<span class="visible-xs"><i class="fa fa-file-o"></i></span>
					<span class="hidden-xs">Materi / Tugas</span>
				</a>
			</li>
			<li class="<?php echo isset($kuliah) ? $kuliah : "" ?>">
				<a href="#home" data-toggle="tab" aria-expanded="true">
					<span class="visible-xs"><i class="fa fa-home"></i></span>
					<span class="hidden-xs">Kuliah & Praktikum</span>
				</a>
			</li>
			<li class="<?php echo isset($uas) ? $uas : "" ?>">
				<a href="#messages" data-toggle="tab" aria-expanded="false">
					<span class="visible-xs"><i class="fa fa-file-text"></i></span>
					<span class="hidden-xs">UTS/UAS</span>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane <?php echo isset($kuliah) ? $kuliah : "" ?>" id="home">
				<h3>Kuliah & Praktikum</h3>
				<span class="pull-right col-lg-2">
					<a href="<?php echo base_url() ?>schedule?download=pdf&schedule=kuliah&semester=<?php echo isset($_GET['semester']) ? $_GET['semester'] : '' ?>" class="btn btn-danger">Download PDF</a>
				</span>
 				<div class="col-lg-3" style="padding-left:0px">
					<form class="form-horizontal" action="<?=base_url();?>schedule/index" method="get">
						<div class="form-group input-group" style="margin:0">
							<select name="semester" class="selectpicker form-control data-input" data-required="true">
								<option value="">-- Pilih Semester --</option>
								<option value="1" <?php echo isset($_GET['semester']) && $_GET['semester'] == '1' ? 'selected' : '' ?>>Semester 1</option>
								<option value="2" <?php echo isset($_GET['semester']) && $_GET['semester'] == '2' ? 'selected' : '' ?>>Semester 2</option>
								<option value="3" <?php echo isset($_GET['semester']) && $_GET['semester'] == '3' ? 'selected' : '' ?>>Semester 3</option>
								<option value="4" <?php echo isset($_GET['semester']) && $_GET['semester'] == '4' ? 'selected' : '' ?>>Semester 4</option>
								<option value="5" <?php echo isset($_GET['semester']) && $_GET['semester'] == '5' ? 'selected' : '' ?>>Semester 5</option>
								<option value="6" <?php echo isset($_GET['semester']) && $_GET['semester'] == '6' ? 'selected' : '' ?>>Semester 6</option>
							</select>
							<span class="input-group-btn">
								<?php echo form_submit(array('class' => 'btn btn-warning', 'value' => 'Cari')); ?>
							</span>
						</div>
					</form>
				</div>
				<div class="clearfix"></div>
				<br>
				<table id="tech-companies-1" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th data-priority="3">Mata Kuliah</th>
							<th data-priority="1">SKS</th>
							<th data-priority="3">Dosen</th>
							<th data-priority="3">Kls</th>
							<th data-priority="3">Ruang/Gedung</th>
							<th data-priority="3">Hari</th>
							<th data-priority="3">Jam</th>
							<th data-priority="3">Tahun Ajaran</th>
							<th>Silabus</th>
						</tr>
					</thead>
					<tbody>
						<?php if($result_data->status == 'ERROR'){ ?>
							<tr>
								<th colspan="10"><center><?php echo $result_data->messages ?></center></th>
							</tr>
						<?php }else{ ?>
							<?php $no=1; foreach ($result_data->results->jadwal as $schedule){?>
								<tr>
									<td><?php echo $no++ ?>.</td>
									<td><?php echo $schedule->matakuliah ?></td>
									<td><?php echo $schedule->sks ?></td>
									<td><?php echo $schedule->dosen ?></td>
									<td><?php echo $schedule->kelas ?></td>
									<td><?php echo $schedule->ruangan ?>/<?php echo $schedule->gedung ?></td>
									<td><?php echo $schedule->hari ?></td>
									<td><?php echo $schedule->jam_mulai ?> s/d <?php echo $schedule->jam_akhir ?></td>
									<td><?php echo $schedule->tahun_ajaran ?></td>
									<td><?php echo $schedule->silabus == "" ? "<span style='color: red'>Kosong</span>" : "<a href=".$schedule->silabus." class='btn btn-white'><i class='fa fa-download></i></a>'" ?></td>
								</tr>
							<?php } ?>
						<?php } ?>
					 </tbody>
				</table>
			</div>
			<!-- <div class="tab-pane" id="profile">
			</div> -->
			<div class="tab-pane <?php echo isset($uas) ? $uas : "" ?>" id="messages">
				<h3>UTS/UAS</h3>
 				<span class="pull-right col-lg-2">
					<a href="<?php echo base_url() ?>schedule?download=pdf&schedule=uas&semester_uas=<?php echo isset($_GET['semester_uas']) ? $_GET['semester_uas'] : '' ?>&jenis=<?php echo isset($_GET['jenis']) ? $_GET['jenis'] : '' ?>" class="btn btn-danger">Download PDF</a>
				</span>
				<form class="form-horizontal" action="<?=base_url();?>schedule/index" method="get">
					<div class="col-lg-2" style="padding-left:0px">
						<input type="hidden" name="schedule" value="uas">
						<select name="semester_uas" class="selectpicker form-control data-input" data-required="true">
							<option value="">-- Pilih Semester --</option>
							<option value="1" <?php echo isset($_GET['semester_uas']) && $_GET['semester_uas'] == '1' ? 'selected' : '' ?>>Semester 1</option>
							<option value="2" <?php echo isset($_GET['semester_uas']) && $_GET['semester_uas'] == '2' ? 'selected' : '' ?>>Semester 2</option>
							<option value="3" <?php echo isset($_GET['semester_uas']) && $_GET['semester_uas'] == '3' ? 'selected' : '' ?>>Semester 3</option>
							<option value="4" <?php echo isset($_GET['semester_uas']) && $_GET['semester_uas'] == '4' ? 'selected' : '' ?>>Semester 4</option>
							<option value="5" <?php echo isset($_GET['semester_uas']) && $_GET['semester_uas'] == '5' ? 'selected' : '' ?>>Semester 5</option>
							<option value="6" <?php echo isset($_GET['semester_uas']) && $_GET['semester_uas'] == '6' ? 'selected' : '' ?>>Semester 6</option>
						</select>
					</div>
					<div class="col-lg-2" style="padding-left:0px">
						<div class="form-group input-group" style="margin:0">
							<select name="jenis" class="selectpicker form-control data-input" data-required="true">
								<option value="">-- Pilih Jenis --</option>
								<option value="1" <?php echo isset($_GET['jenis']) && $_GET['jenis'] == '1' ? 'selected' : '' ?>>UTS</option>
								<option value="2" <?php echo isset($_GET['jenis']) && $_GET['jenis'] == '2' ? 'selected' : '' ?>>UAS</option>
							</select>
							<span class="input-group-btn">
								<?php echo form_submit(array('class' => 'btn btn-warning', 'value' => 'Cari')); ?>
							</span>
						</div>
					</div>
				</form>
				<div class="clearfix"></div>
				<br>
				<table id="tech-companies-1" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th data-priority="3">Mata Kuliah</th>
							<th data-priority="1">Tanggal</th>
							<th data-priority="3">Jam</th>
							<th data-priority="3">Ruang/Gedung</th>
							<th data-priority="3">Tahun Ajaran</th>
						</tr>
					</thead>
					<tbody>
						<?php if($result_data_uas->status == 'ERROR'){ ?>
							<tr>
								<th colspan="9"><center><?php echo $result_data_uas->messages ?></center></th>
							</tr>
						<?php }else{ ?>
							<?php $no=1; foreach ($result_data_uas->results->jadwal as $schedule){?>
								<tr>
									<td><?php echo $no++ ?>.</td>
									<td><?php echo $schedule->matakuliah ?></td>
									<td><?php echo $schedule->tanggal ?></td>
									<td><?php echo $schedule->jam_mulai ?> s/d <?php echo $schedule->jam_akhir ?></td>
									<td><?php echo $schedule->ruangan ?>/<?php echo $schedule->gedung ?></td>				
									<td><?php echo $schedule->tahun_ajaran ?></td>
								</tr>
							<?php } ?>
						<?php } ?>
					 </tbody>
				</table>
			</div>

			<div class="tab-pane <?php echo isset($tugas) ? $tugas : "" ?>" id="tugas">
				<h3>Materi/Tugas Kuliah</h3>
				<span class="pull-right col-lg-2">
					<a href="<?php echo base_url() ?>schedule?download=pdf&schedule=tugas&semester_tugas=<?php echo isset($_GET['semester_tugas']) ? $_GET['semester_tugas'] : '' ?>&tanggal=<?php echo isset($_GET['tanggal']) ? $_GET['tanggal'] : '' ?>" class="btn btn-danger">Download PDF</a>
				</span>
				<form class="form-horizontal" action="<?=base_url();?>schedule/index" method="get">
					<div class="col-lg-2" style="padding-left:0px">
						<input type="hidden" name="schedule" value="tugas">
						<select name="semester_tugas" class="selectpicker form-control data-input" data-required="true">
							<option value="">-- Pilih Semester --</option>
							<option value="1" <?php echo isset($_GET['semester_tugas']) && $_GET['semester_tugas'] == '1' ? 'selected' : '' ?>>Semester 1</option>
							<option value="2" <?php echo isset($_GET['semester_tugas']) && $_GET['semester_tugas'] == '2' ? 'selected' : '' ?>>Semester 2</option>
							<option value="3" <?php echo isset($_GET['semester_tugas']) && $_GET['semester_tugas'] == '3' ? 'selected' : '' ?>>Semester 3</option>
							<option value="4" <?php echo isset($_GET['semester_tugas']) && $_GET['semester_tugas'] == '4' ? 'selected' : '' ?>>Semester 4</option>
							<option value="5" <?php echo isset($_GET['semester_tugas']) && $_GET['semester_tugas'] == '5' ? 'selected' : '' ?>>Semester 5</option>
							<option value="6" <?php echo isset($_GET['semester_tugas']) && $_GET['semester_tugas'] == '6' ? 'selected' : '' ?>>Semester 6</option>
						</select>
					</div>
					<div class="col-lg-2" style="padding-left:0px">
						<div class="form-group input-group" style="margin:0">
							<input type="text" class="form-control" placeholder="yyyy-mm-dd" id="datepicker-autoclose" name="tanggal" value="<?php echo isset($_GET['tanggal']) ? $_GET['tanggal'] : '' ?>">
							<span class="input-group-btn">
								<?php echo form_submit(array('class' => 'btn btn-warning', 'value' => 'Cari')); ?>
							</span>
						</div>
					</div>
				</form>
				<div class="clearfix"></div>
				<br>
				<table id="tech-companies-1" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="5%">No</th>
							<th data-priority="3">Mata Kuliah</th>
							<th data-priority="1">Tanggal</th>
							<th>Tipe</th>
							<th>File</th>
							<th>Deskripsi</th>
						</tr>
					</thead>
					<tbody>
						<?php if($result_data_tugas->status == 'ERROR'){ ?>
							<tr>
								<th colspan="6"><center><?php echo $result_data_tugas->messages ?></center></th>
							</tr>
						<?php }else{ ?>
							<?php $no=1; foreach ($result_data_tugas->results->materi as $schedule){?>
								<tr>
									<td><?php echo $no++ ?>.</td>
									<td><?php echo $schedule->matakuliah ?></td>
									<td><?php echo $schedule->tanggal ?></td>
									<td><?php echo $schedule->materi->tipe ?></td>
									<td><a href="<?php echo $schedule->materi->file ?>">
										<?php echo $schedule->materi->judul ?>
									</a></td>
									<td><?php echo $schedule->materi->deskripsi == "" ? "-" : $schedule->materi->deskripsi ?></td>
								</tr>
							<?php } ?>
						<?php } ?>
					 </tbody>
				</table>
			</div>

		</div>
	</div>
</div>