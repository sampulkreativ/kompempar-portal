<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	function cim()
	{
	    $cim =& get_instance();
	    return $cim;

	}

	function h_Menu(){
		$data = array();
		$ci = cim();
		$ci->system = $ci->config->item('system');

		$data['client_id'] 				= $ci->system['apiClientId'];
		$data['client_secret'] 			= $ci->system['apiClientSecret'];
		$data['token'] 					= $ci->session->userdata('token');

		$jwt 							= $ci->jwt->encode($data, $ci->system['jwtKey']);
		$data['result'] 				= $ci->curl->simple_get($ci->system['apiUrl'] .'master/travel_categories', array("jwt" => $jwt));

		$data['result_menu'] 			= json_decode($data['result']);

		return $data['result_menu'];
	}

	function h_point(){

		$data = array();
		$ci = cim();

		$ci->system = $ci->config->item('system');

		$data['client_id'] 				= $ci->system['apiClientId'];
		$data['client_secret'] 			= $ci->system['apiClientSecret'];
		$data['token'] 					= $ci->session->userdata('token');

		$jwt 							= $ci->jwt->encode($data, $ci->system['jwtKey']);
		$data['result'] 				= $ci->curl->simple_get($ci->system['apiUrl'] .'master/point_types', array("jwt" => $jwt));

		$data['result_point'] 		= json_decode($data['result']);

			
			return $data['result_point'];
	}

?>