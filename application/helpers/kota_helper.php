<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	function cikota()
	{
	    $ci =& get_instance();
	    return $ci;

	}

	function h_kotakabupaten($id){

		$data = array();
		$ci = cikota();

		$data['client_id'] 				= $ci->system['apiClientId'];
		$data['client_secret'] 			= $ci->system['apiClientSecret'];
		$data['token'] 					= $ci->session->userdata('token');
			
		$data["id"]						= "";
		$data["province_id"]			= $id;
		$jwt 							= $ci->jwt->encode($data, $ci->system['jwtKey']);
		$data['result'] 				= $ci->curl->simple_get($ci->system['apiUrl'] .'master/cities', array("jwt" => $jwt));
			
		echo $data['result'];

	}


	
?>