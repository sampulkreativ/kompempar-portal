<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	function cip()
	{
	    $cip =& get_instance();
	    return $cip;

	}

	function callProvinces(){
		$data = array();
		$ci = cip();

		$ci->system = $ci->config->item('system');

		$data['client_id'] 				= $ci->system['apiClientId'];
		$data['client_secret'] 			= $ci->system['apiClientSecret'];
		$data['token'] 					= $ci->session->userdata('token');
		$data["id"]						= "";
		$jwt 							= $ci->jwt->encode($data, $ci->system['jwtKey']);
		$data['result'] 				= $ci->curl->simple_get($ci->system['apiUrl'] .'master/provinces', array("jwt" => $jwt));

		$data['result_provinsi'] 		= json_decode($data['result']);

			
		return $data['result_provinsi'];
	}


	
?>