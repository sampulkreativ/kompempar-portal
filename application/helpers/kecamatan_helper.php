<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	function cikec()
	{
	    $ci =& get_instance();
	    return $ci;

	}

	function h_kecamatan($id){
		$data = array();
		$ci = cikec();

		$ci->system = $ci->config->item('system');

		$data['client_id'] 				= $ci->system['apiClientId'];
		$data['client_secret'] 			= $ci->system['apiClientSecret'];
		$data['token'] 					= $ci->session->userdata('token');
			

		$data["id"]						= "";
		$data["city_id"]				= $id;
		$jwt 							= $ci->jwt->encode($data, $ci->system['jwtKey']);
		$data['result'] 				= $ci->curl->simple_get($ci->system['apiUrl'] .'master/villages', array("jwt" => $jwt));
			
		echo $data['result'];
	}

	function h_travels($city_id){
		$data = array();
		$ci = cikec();

		$ci->system = $ci->config->item('system');

		$data['client_id'] 				= $ci->system['apiClientId'];
		$data['client_secret'] 			= $ci->system['apiClientSecret'];
		$data['token'] 					= $ci->session->userdata('token');
			

		$data["id"]						= "";
		$data["city_id"]				= $city_id;
		$jwt 							= $ci->jwt->encode($data, $ci->system['jwtKey']);
		$data['result'] 				= $ci->curl->simple_get($ci->system['apiUrl'] .'travel/travels_list', array("jwt" => $jwt));
			
		echo $data['result'];
	}


	
?>