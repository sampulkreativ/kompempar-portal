<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	function ci()
	{
	    $ci =& get_instance();
	    return $ci;

	}

	function find_audit_results($question_id, $object_id, $point_type_id, $point_id, $type, $limit=null, $offset=null){
		$data = array();
		$ci = ci();
		$ci->system = $ci->config->item('system');
		$data['client_id'] 			= $ci->system['apiClientId'];
		$data['client_secret'] 	= $ci->system['apiClientSecret'];			  
    $data['object_id']    	= $object_id;
    $data['question_id'] 		= $question_id;
    $data['point_id'] 		= $point_id;
    $data['point_type_id'] 		= $point_type_id;
    $data['limit'] 					= $limit;
    $data['offset'] 				= $offset;
    $data['type'] 					= $type;

    $jwt 										= $ci->jwt->encode($data, $ci->system['jwtKey']);
		$data['result'] 				= $ci->curl->simple_get($ci->system['apiUrl'] .'/tourism/get_point', array("jwt" => $jwt));
		$data['result_data'] 		= json_decode($data['result']);
		
		$point = $data['result_data']->results->audit_results[0]->point;
		return $point == null ? 0 : $point;
	}	
?>