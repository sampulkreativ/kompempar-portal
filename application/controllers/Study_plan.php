<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Study_plan extends CI_Controller {
	
	public $template = array();
	public $data = array();

	public function __construct(){

		parent::__construct();

		$this->system = $this->config->item('system');

	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	public function index(){

		if ($this->session->userdata('token') == "") {
			redirect("/");
		}
		else{
			
			$data['client_id'] 		= $this->system['apiClientId'];
			$data['client_secret'] 	= $this->system['apiClientSecret'];
			$data['token'] 			= $this->session->userdata('token');

			$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 		= $this->curl->simple_get($this->system['apiUrl'] .'/krs/periode', array("jwt" => $jwt));

			$data['result_krs'] 	= $this->curl->simple_get($this->system['apiUrl'] .'/krs/matkul', array("jwt" => $jwt));
			$result_array 			= json_decode($data['result']);
			$result_array_krs 		= json_decode($data['result_krs']);

			if (isset($_GET['history'])) {
				$data['semester'] 	= $this->input->get('semester');
				$jwt_history 		= $this->jwt->encode($data, $this->system['jwtKey']);
				$data['result_krs_histories'] 	= $this->curl->simple_get($this->system['apiUrl'] .'/krs/riwayat', array("jwt" => $jwt_history));
				$result_array_krs_histories 	= json_decode($data['result_krs_histories']);
			}

			$data = array(
				'page_title' 		=> 'Rencana Pembelajaran',
				'description' 		=> 'Rencana Pembelajaran Mahasiswa',
				'title' 			=> 'Rencana Pembelajaran - Mahasiswa Akfis Dustira',
				'active_krs' 		=> 'active_tab',
				'result_array' 		=> $result_array,
				'result_data' 		=> $result_array->results,
				'result_array_krs' 	=> $result_array_krs->results
			);
			if (isset($_GET['history'])) {
				$data['result_histories'] = $result_array_krs_histories->results;
			}

			if ($result_array->messages == "Authentication Failed") {
				redirect("/logout");
			}else{
				if(isset($_GET['download'])){
					$this->load->library('r_pdf');
					$html=$this->load->view('study_plan/show_pdf',$data, true);
					$pdfFilePath = "Rencana Belajar-".time().".pdf";
					$pdf = $this->r_pdf->load();
					$pdf->WriteHTML($html,2);
					$pdf->Output($pdfFilePath, "D");
				}
				$this->middle = 'study_plan/index';
				$this->data = $data;
				$this->layout();
			}
		}
	}

	public function create(){	

		if ($this->session->userdata('token') == "") {
			redirect("/");
		}
		else{			
			$data['client_id'] 		= $this->system['apiClientId'];
			$data['client_secret'] 	= $this->system['apiClientSecret'];
			$data['token'] 			= $this->session->userdata('token');
			$data['matkul'] 		= implode(",",$this->input->post('matkul[]', true));
			$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 		= $this->curl->simple_post($this->system['apiUrl'] .'/krs/simpan', array("jwt" => $jwt));
			$result_array 			= json_decode($data['result']);

			if ($result_array->messages == "Authentication Failed") {
				redirect("/logout");
			}else{				
				redirect("/study_plan?notice=".$result_array->results->krs->pesan);
			}
		}
	}

	public function backward(){

		if ($this->session->userdata('token') == "") {
			redirect("/");
		}
		else{
			
			$data['client_id'] 		= $this->system['apiClientId'];
			$data['client_secret'] 	= $this->system['apiClientSecret'];
			$data['matkul'] 		= $this->uri->segment(3);
			$data['token'] 			= $this->session->userdata('token');

			$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 		= $this->curl->simple_post($this->system['apiUrl'] .'/krs/mundur', array("jwt" => $jwt));
			$result_array 			= json_decode($data['result']);

			if ($result_array->messages == "Authentication Failed") {
				redirect("/logout");
			}else{
				redirect("/study_plan?notice=".$result_array->results->krs->pesan);
			}
		}
	}

	public function history(){

		if ($this->session->userdata('token') == "") {
			redirect("/");
		}
		else{
			
			$data['client_id'] 		= $this->system['apiClientId'];
			$data['client_secret'] 	= $this->system['apiClientSecret'];
			$data['semester'] 		= $this->input->get('semester');
			$data['token'] 			= $this->session->userdata('token');

			$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 		= $this->curl->simple_get($this->system['apiUrl'] .'/krs/riwayat', array("jwt" => $jwt));
			$result_array 			= json_decode($data['result']);

			$data = array(
				'page_title' 	=> 'Rencana Pembelajaran',
				'description' 	=> 'Rencana Pembelajaran Mahasiswa',
				'title' 		=> 'Rencana Pembelajaran - Mahasiswa Akfis Dustira',
				'active_krs' 	=> 'active_tab',
				'result_data' 	=> $result_array->results,
			);
			if ($result_array->messages == "Authentication Failed") {
				redirect("/logout");
			}else{
				$this->middle = 'study_plan/index';
				$this->data = $data;
				$this->layout();
			}
		}
	}
}
