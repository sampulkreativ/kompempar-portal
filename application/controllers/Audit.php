<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit extends CI_Controller {
	
	public $template = array();
	public $data = array();

	public function __construct(){

		parent::__construct();

		$this->load->helper('Menu');
		$this->load->helper('Provinsi');
		$this->load->helper('Kota');
		$this->load->helper('Kecamatan');
		$this->system = $this->config->item('system');
	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/header-portal', $this->template);
	}

	public function index(){

		// if ($this->session->userdata('token') == "") {
		// 	redirect("/");
		// }
		// else{
			
			$data = array(
				'page_title' 	=> 'Hasil Audit',
				'description' 	=> 'Informasi Hasil Audit',
				'title' 		=> 'Hasil audit objek wisata'				
			);

			$id_prov = "";
			$id_kota = "";
			$id_kec  = "";
			$id_point  = "";
			$id_kategori  = "";
			$data["history_point"] = "";
			$data["history_provinsi"] = "";
			$data["history_kota"] = "";
			$data["history_kecamatan"] = "";
			$data["history_kategori"] = "";

			if($this->uri->segment(1)=="audit"){
				if(isset($_GET["kategori"])){
					$id_kategori		 = $_GET["kategori"];
					$this->session->set_userdata('sess_search_kategori', $id_kategori);
					$data["history_kategori"] = $id_kategori;
				} else{
					$id_kategori = $this->session->userdata('sess_search_kategori');
					$data["history_kategori"] = $id_kategori;
				}

				if(isset($_GET["point"])){
					$id_point		 = $_GET["point"];
					$this->session->set_userdata('sess_search_point', $id_point);
					$data["history_point"] = $id_point;
				} else{
					$id_point = $this->session->userdata('sess_search_point');
					$data["history_point"] = $id_point;
				}

				if(isset($_GET["provinsi"])){
					$id_prov		 = $_GET["provinsi"];
					$this->session->set_userdata('sess_search_prov', $id_prov);
					$data["history_provinsi"] = $id_prov;
				} else{
					$id_prov = $this->session->userdata('sess_search_prov');
					$data["history_provinsi"] = $id_prov;
				}

				if(isset($_GET["kotakabupaten"])){
					$id_kota		 = $_GET["kotakabupaten"];
					$this->session->set_userdata('sess_search_kota', $id_kota);
					$data["history_kota"] = $id_kota;
				} else{
					$id_kota = $this->session->userdata('sess_search_kota');
					$data["history_kota"] = $id_kota;
				}

				if(isset($_GET["kecamatan"])){
					$id_kec			 = $_GET["kecamatan"];
					$this->session->set_userdata('sess_search_kec', $id_kec);
					$data["history_kecamatan"] = $id_kec;
				} else{
					$id_kec = $this->session->userdata('sess_search_kec');
					$data["history_kecamatan"] = $id_kec;
				}

				if(isset($_GET["traveling_id"])){
					$traveling_id			 = $_GET["traveling_id"];
					$this->session->set_userdata('sess_search_objek', $traveling_id);
					$data["history_traveling_id"] = $traveling_id;
				} else{
					$traveling_id = $this->session->userdata('sess_search_objek');
					$data["history_traveling_id"] = $traveling_id;
				}

			} else{
				$this->session->unset_userdata('history_point');
				$this->session->unset_userdata('history_provinsi');
				$this->session->unset_userdata('history_kota');
				$this->session->unset_userdata('history_kecamatan');
				$this->session->unset_userdata('history_traveling_id');
				$this->session->unset_userdata('history_kategori');
			}

			//echo $data["history_kota"]."hahaha";
			
			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');
			$data['limit']     				= "";//"10";
		    $data['offset']     			= $this->input->get('offset') == "" ? 0 : $this->input->get('offset');
		    $data['province_id']     		= $this->input->get('provinsi');
		    $data['city_id']     			= $this->input->get('kotakabupaten');
		    $data['village_id']     		= $this->input->get('kecamatan');
		    $data['traveling_id']     		= $this->input->get('traveling_id');
		    $data['travel_category_id']    	= $this->input->get('kategori');
		    $data['sub_travel_category_id'] = $this->input->get('subkategori');
		    $data['national_id'] 			= $this->input->get('pencarian');
		    $data['point_type_id'] 			= $this->input->get('point');
		    $data['group'] 					= true;		    


			$data['result_menu'] 			= h_Menu();
			$data['result_provinsi'] 		= callProvinces();
			$data['result_point'] 			= h_point();
			
			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'/question/questions_list', array("jwt" => $jwt));
 			
			$data['result_object'] 			= $this->curl->simple_get($this->system['apiUrl'] .'/tourism/audit_results', array("jwt" => $jwt));	

			//print_r($data['result_object']);
		
			
			$data['result_data_questions'] 	= json_decode($data['result']);
			$data['result_data_objects'] 	= json_decode($data['result_object']);		

			if(!empty($_GET["submit"]) && !empty($_GET["traveling_id"])){
				redirect("/page/objek/".$data['history_traveling_id']."", "refresh");
			} else if($data["history_traveling_id"] != 0){
				$this->middle = 'frontend/audit';
					$this->data = $data;
					$this->layout();
			}else{
				if($this->input->get('request') != "script"){
					$this->middle = 'frontend/audit';
					$this->data = $data;
					$this->layout();
				}else{
					$this->load->view('frontend/audit_body_table', $data);
				}
			}
			
		//}

	}
}