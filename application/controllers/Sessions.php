<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sessions extends CI_Controller {
	
	public $template = array();
	public $data = array();

	public function __construct(){

		parent::__construct();
		$this->load->helper('url');
		$this->system = $this->config->item('system');
	}

	public function layout(){

		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/portal', $this->template);

	}

	public function index(){

		if ($this->session->userdata('token') != "") {
			redirect("/");
		}

		$this->middle = 'session/index';
		$this->layout();

	}

	public function register(){

		$this->middle = 'session/register';
		$this->layout();

	}

	public function create_register(){
		$data['client_id'] 		= $this->system['apiClientId'];
		$data['client_secret'] 	= $this->system['apiClientSecret'];
		$data['email'] 			= $this->input->post('email', true);
		$data['password'] 		= $this->input->post('password', true);
		$data['full_name'] 		= $this->input->post('full_name', true);

		$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
		$data['result'] 		= $this->curl->simple_post($this->system['apiUrl'] .'/user/register', array("jwt" => $jwt));
		$result_array 			= json_decode($data['result']);

		if ($result_array->status == "ERROR") {
			$data['alert'] = $result_array->messages;
			$this->middle = 'session/register';			
		}
		else{			
			$this->session->set_userdata(array("token" => $result_array->results->register->token));
			$data['notice'] = $result_array->results->register->message;
			$this->middle = 'session/register';
		}

		$this->data = $data;
		$this->layout();
	}

	public function create(){

		$data['client_id'] 		= $this->system['apiClientId'];
		$data['client_secret'] 	= $this->system['apiClientSecret'];
		$data['email'] 			= $this->input->post('username', true);
		$data['password'] 		= $this->input->post('password', true);

		$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
		$data['result'] 		= $this->curl->simple_post($this->system['apiUrl'] .'/user/login', array("jwt" => $jwt));
		$result_array 			= json_decode($data['result']);

		if ($result_array->status == "ERROR") {
			$data['alert'] = $result_array->messages;
			$this->middle = 'session/index';
		}
		else{
			$this->session->set_userdata(array("token" => $result_array->results->login->token));
			redirect('http://member.kompepar.or.id/members?z='.$result_array->results->login->token);
		}

		$this->data = $data;
		$this->layout();

	}

	public function forget_password(){

		$this->middle = 'session/forget_password';
		$this->layout();

	}

	public function process_forget(){

		$data['client_id'] 		= $this->system['apiClientId'];
		$data['client_secret'] 	= $this->system['apiClientSecret'];
		$data['email'] 			= $this->input->post('email', true);

		$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
		$data['result'] 			= $this->curl->simple_post($this->system['apiUrl'] .'/user/lupasandi', array("jwt" => $jwt));
		$result_array 			= json_decode($data['result']);

		if ($result_array->status == "ERROR") {
			$data['alert'] = $result_array->messages;	
		}
		else{
			$data['notice'] = 'Permintaan telah berhasil dikirim';	
		}
		$this->middle = 'session/forget_password';
		$this->data = $data;
		$this->layout();
	}

	public function change_password($key){

		$this->middle = 'session/change_password';
		$data['key'] = $key;
		$this->data = $data;
		$this->layout();

	}

	public function process_change_password($key){

		$data['client_id'] 		= $this->system['apiClientId'];
		$data['client_secret'] 	= $this->system['apiClientSecret'];
		$data['sandi_baru'] 	= $this->input->post('new_password', true);
		$data['kunci'] 			= $key;

		$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
		$data['result'] 			= $this->curl->simple_post($this->system['apiUrl'] .'/user/resetsandi', array("jwt" => $jwt));
		$result_array 			= json_decode($data['result']);
		
		if ($result_array->status != "ERROR") {
			redirect('/login');
		}else{
			$data['key'] 		= $key;
			$data['alert'] 		= $result_array->messages;
			$this->data 		= $data;
			$this->middle 		= 'session/change_password';
			$this->layout();
		}

	}

	public function destroy(){		
		$this->session->unset_userdata(array("token"));
		$this->session->sess_destroy();
		redirect('/');

	}
}
