<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Study extends CI_Controller {
	
	public $template = array();
	public $data = array();

	public function __construct(){

		parent::__construct();

		$this->system = $this->config->item('system');

	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	public function index(){

		$data = array(
			'page_title' 	=> 'Hasil Belajar',
			'description' 	=> 'Informasi Hasil Belajar Mahasiswa',
			'title' 			=> 'Hasil Belajar - Mahasiswa Akfis Dustira',
			'result_study' 	=> 'active_tab'
		);

		$data['client_id'] 		= $this->system['apiClientId'];
		$data['client_secret'] 	= $this->system['apiClientSecret'];
		$data['token'] 			= $this->session->userdata('token');
		$data['semester'] 		= $this->input->get('semester');

		$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
		$data['result'] 			= $this->curl->simple_get($this->system['apiUrl'] .'/nilai', array("jwt" => $jwt));
		$data['result_transkrip']	= $this->curl->simple_get($this->system['apiUrl'] .'/nilai/transkip', array("jwt" => $jwt));

		$data['result_data'] 			= json_decode($data['result']);
		$data['result_data_transkrip'] 	= json_decode($data['result_transkrip']);

		if ($data['result_data']->messages == "Authentication Failed") {
			redirect("/logout");
		}else{
			if(isset($_GET['download'])){
				$this->load->library('r_pdf');
				$html=$this->load->view('study/show_pdf',$data, true);
				$pdfFilePath = "Hasil Belajar-".time().".pdf";
				$pdf = $this->r_pdf->load();
				$pdf->WriteHTML($html,2);
				$pdf->Output($pdfFilePath, "D");
			}
			$this->middle 	= 'study/index';
			$this->data 	= $data;
			$this->layout();
		}
	}
}
