<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Finance extends CI_Controller {
	
	public $template = array();
	public $data = array();

	public function __construct(){

		parent::__construct();

		$this->system = $this->config->item('system');
	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	public function index(){

		if ($this->session->userdata('token') == "") {
			redirect("/");
		}
		else{
			
			$data = array(
				'page_title' 	=> 'Keuangan',
				'description' 	=> 'Informasi Keuangan Mahasiswa',
				'title' 			=> 'Keuangan - Mahasiswa Akfis Dustira',
				'finance' 		=> 'active_tab'
			);
			
			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');

			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 					= $this->curl->simple_get($this->system['apiUrl'] .'/keuangan/transaksi', array("jwt" => $jwt));
			$data['result_pembayaran'] 	= $this->curl->simple_get($this->system['apiUrl'] .'/keuangan/pembayaran', array("jwt" => $jwt));

			$data['result_data'] 				= json_decode($data['result']);
			$data['result_data_pembayaran'] 	= json_decode($data['result_pembayaran']);

			if ($data['result_data']->messages == "Authentication Failed") {
				redirect("/logout");
			}else{
				$this->middle = 'finance/index';
				$this->data = $data;
				$this->layout();
			}
		}

	}

	public function create(){

		$photoTmpPath 	= $_FILES['bukti']['tmp_name'];
		$datas 				= file_get_contents($photoTmpPath);
		$base64 			= base64_encode($datas);

		$data['client_id'] 		= $this->system['apiClientId'];
		$data['client_secret'] 	= $this->system['apiClientSecret'];
		$data['token'] 			= $this->session->userdata('token');
		$data['gambar'] 		= $base64;

		$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
		$data['result'] 			= $this->curl->simple_post($this->system['apiUrl'] .'/keuangan/konfirmasi', array("jwt" => $jwt));
		$result_array 			= json_decode($data['result']);

		if ($result_array->messages == 'Success') {
			redirect("/finance?notice=".$result_array->results->konfirmasi->pesan);
		}else{
			redirect('welcome?notice=API Error');
		}
	}
}
