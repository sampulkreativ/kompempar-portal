<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
	
	public $template = array();
	public $data = array();
	public $data_profile = array();

	public function __construct(){

		parent::__construct();

		$this->system = $this->config->item('system');

	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	public function index(){
		
		if ($this->session->userdata('token') == "") {
			redirect("/");
		}
		else{
			$data['client_id'] 		= $this->system['apiClientId'];
			$data['client_secret'] 	= $this->system['apiClientSecret'];
			$data['token'] 			= $this->session->userdata('token');

			$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 			= $this->curl->simple_get($this->system['apiUrl'] .'/user/profil', array("jwt" => $jwt));
			$result_array 			= json_decode($data['result']);

			$data['result_province'] = $this->curl->simple_get($this->system['apiUrl'].'/provinsi', array("jwt" => $jwt));
			$result_province_array = json_decode($data['result_province']);

			$data['result_city'] = $this->curl->simple_get($this->system['apiUrl'].'/provinsi/kota', array("jwt" => $jwt));
			$result_city_array = json_decode($data['result_city']);

			$data = array(
				'page_title' 	=> 'Pengaturan',
				'description' 	=> 'Atur Halaman Mahasiswa Anda',
				'result_data' 	=> $result_array->results,
				'result_provinces' => $result_province_array->results,
				'result_cities' => $result_city_array->results,
				'title' 		=> 'Pengaturan - Mahasiswa Akfis Dustira',
				'setting' 		=> 'active_tab'
			);
			if ($result_array->messages == "Authentication Failed") {
				redirect("/logout");
			}else{
				$this->middle 	= 'setting/index';
				$this->data 	= $data;
				$this->layout();
			}
		}
	}

	public function update_password(){
		$password 				= $this->input->post('password', true);
		$conf_password 		= $this->input->post('conf_password', true);

		$data['page_title'] 	= 'Pengaturan';
		$data['description'] = 'Atur Portal Mahasiswa Anda';

		if ($password == $conf_password) {
			
			$data['client_id'] 		= $this->system['apiClientId'];
			$data['client_secret'] 	= $this->system['apiClientSecret'];
			$data['token'] 			= $this->session->userdata('token');
			$data['sandi_lama'] 	= $this->input->post('password_lama', true);
			$data['sandi_baru'] 	= $password;

			$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 		= $this->curl->simple_post($this->system['apiUrl'] .'/user/ubahsandi', array("jwt" => $jwt));
			$result_array 			= json_decode($data['result']);

			if ($result_array->messages == "Authentication Failed") {
				redirect("/logout");
			}else{
				if ($result_array->status == 'ERROR') {
					redirect("/setting?notice=".$result_array->messages);
				}
				else{
					redirect("/setting?notice=".$result_array->results->ubahsandi->pesan);
				}
			}
		}
		else{
			$data['errors'] = ['Konfirmasi Password tidak sesuai'];
			redirect("/setting?notice=Konfirmasi Password tidak sesuai");
		}
	}
		
	public function update_profile(){
		if($_FILES['photo']['tmp_name'] != null){
			$photoTmpPath 	= $_FILES['photo']['tmp_name'];
			$datas 				= file_get_contents($photoTmpPath);
			$base64 			= base64_encode($datas);

			$data['client_id'] 		= $this->system['apiClientId'];
			$data['client_secret'] 	= $this->system['apiClientSecret'];
			$data['token'] 			= $this->session->userdata('token');
			$data['foto'] 			= $base64;
			
			$jwt 			= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 	= $this->curl->simple_post($this->system['apiUrl'] .'/user/ubahfoto', array("jwt" => $jwt));
		}

		$data_profile['client_id'] 		= $this->system['apiClientId'];
		$data_profile['client_secret'] 	= $this->system['apiClientSecret'];
		$data_profile['token'] 			= $this->session->userdata('token');
		$data_profile['nik'] = $this->input->post('nik');
		$data_profile['tempat_lahir'] = $this->input->post('tempat_lahir');
		$data_profile['tanggal_lahir'] = $this->input->post('tahun')."-".$this->input->post('bulan')."-".$this->input->post('tanggal');
		$data_profile['jenis_kelamin'] = $this->input->post('jenis_kelamin');
		$data_profile['agama'] = $this->input->post('agama');
		$data_profile['no_telp'] = $this->input->post('no_telp');
		$data_profile['kota'] = $this->input->post('kota');
		$data_profile['email'] = $this->input->post('email');

		$jwt_profile 	= $this->jwt->encode($data_profile, $this->system['jwtKey']);
		$data['result_profile'] 	= $this->curl->simple_post($this->system['apiUrl'] .'/user/ubahprofil ', array("jwt" => $jwt_profile));

		

		if ($data['result']->messages == "Authentication Failed") {
				redirect("/logout");
		}else{
			if ($data['result']->status == 'ERROR' || $data['result_profile']->status == 'ERROR') {
				redirect("/setting?notice=".$data['result']->messages);
			}
			else{
				redirect("/setting?notice=Profil berhasil dirubah");
			}
		}		
	}
}
