<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public $template = array();
	public $data = array();

	public function __construct(){
		parent::__construct();
		$this->system = $this->config->item('system');
	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	public function index()
	{		

		if ($this->session->userdata('token') == "") {
			redirect("/");
		}else{

			$data['client_id'] 		= $this->system['apiClientId'];
			$data['client_secret'] 	= $this->system['apiClientSecret'];
			$data['token'] 			= $this->session->userdata('token');

			$jwt 				= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 	= $this->curl->simple_get($this->system['apiUrl'] .'/user/profil', array("jwt" => $jwt));
			$result_array 		= json_decode($data['result']);

			if ($result_array->status != 'ERROR' || $result_array->messages != "Authentication Failed") {
				$data = array(
					'page_title' => 'Beranda',
					'description' => 'Sistem Informasi Akademik untuk Mahasiswa',
					'result_data' => $result_array->results,
					'title' => 'Mahasiswa Akfis Dustira',
					'home' => 'active_tab'
				);
				$this->session->set_userdata(array("foto" => $result_array->results->profil->foto));
				$this->session->set_userdata(array("nim" => $result_array->results->profil->nim));
				$this->session->set_userdata(array("nama" => $result_array->results->profil->nama));
				$this->session->set_userdata(array("semester" => $result_array->results->profil->semester));
				$this->middle = 'welcome/index';
				$this->data = $data;
				$this->layout();
			}else{
				redirect("/logout");
			}
		}
	}
}
