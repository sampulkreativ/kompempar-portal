<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	
	public $template = array();
	public $data = array();

	public function __construct(){

		parent::__construct();
		$this->load->helper('menu');
		$this->load->helper('provinsi');
		$this->load->helper('url');
		$this->load->library('pagination');
		$this->system = $this->config->item('system');
	}

	/*public function callMenu(){
			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');

			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'master/travel_categories', array("jwt" => $jwt));

			$data['result_menu'] 			= json_decode($data['result']);

			return $data['result_menu'];
	}


	public function callProvinces(){
			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');
			$data["id"]						= "";
			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'master/provinces', array("jwt" => $jwt));

			$data['result_provinsi'] 		= json_decode($data['result']);

			
			return $data['result_provinsi'];
	}

	public function callPoint(){
			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');

			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'master/point_types', array("jwt" => $jwt));

			$data['result_point'] 		= json_decode($data['result']);

			
			return $data['result_point'];
	}



	public function kotakabupaten(){
			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');
			
			$id=$this->input->post('id');

			$data["id"]						= "";
			$data["province_id"]			= $id;
			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'master/cities', array("jwt" => $jwt));
			
			echo $data['result'];

	}

	public function kecamatan(){
			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');
			
			$id=$this->input->post('id');

			$data["id"]						= "";
			$data["city_id"]				= $id;
			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'master/villages', array("jwt" => $jwt));
			
			echo $data['result'];

	}*/


	public function layout(){
		$this->data['result_menu'] 		= h_menu();


		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/header-portal', $this->template);		
	}

	public function index(){

		$data = array();

		$namaKategori    = $this->uri->segment(3);
		$uri_kategori    = $this->uri->segment(4);
		$uri_subkategori = $this->uri->segment(5);

 			$data['travel_category_id']     = $uri_kategori;
		    $data['sub_travel_category_id'] = $uri_subkategori;
		    $data['limit']     				= "6";
		    $data['offset']     			= "";
		    $data['province_id']     		= "";
		    $data['city_id']     			= "";
		    $data['village_id']     		= "";

			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');
			$data["history_provinsi"] = "";
			$data["history_kota"] = "";
			$data["history_kecamatan"] = "";
			$data["history_traveling_id"] = "";
			$data["history_kategori"] = "";

			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'/travel/travels_list', array("jwt" => $jwt));


			$data['result_data'] 			= json_decode($data['result']);
			

			if ($data['result_data']->messages == "Authentication Failed") {
				redirect("/logout");
			}else{
				
				$this->middle = 'frontend/index';
				$this->data = $data;
				$this->layout();
			}

	}



	public function kategori(){

			$data = array();

			$namaKategori    = $this->uri->segment(3);

			$uri_kategori    = $this->uri->segment(4);
			$uri_subkategori = $this->uri->segment(5);
			$from 			 = $this->uri->segment(6);

			$keyword = "";

			if(!empty($_GET["keyword"])){
				$keyword = $_GET["keyword"];	
				$this->session->set_userdata('sess_search_key', $keyword);
			} else{

				$this->session->unset_userdata('sess_search_key');

			}

			$data = array(
					'page_title' 	=> $namaKategori ,
					'title' 		=> 'Destinasi - '.$namaKategori
				);

			$id_prov = "";
			$id_kota = "";
			$id_kec  = "";
			$data["history_provinsi"] = "";
			$data["history_kota"] = "";
			$data["history_kecamatan"] = "";
			$data["history_traveling_id"] = "";
			$data["history_kategori"] = "";

			if(is_numeric($namaKategori) || empty($namaKategori)){
				$from 			 = $this->uri->segment(3);

				if(isset($_GET["provinsi"])){
					$id_prov		 = $_GET["provinsi"];
					$this->session->set_userdata('sess_search_prov', $id_prov);
					$data["history_provinsi"] = $id_prov;
				} else{
					$id_prov = $this->session->userdata('sess_search_prov');
					$data["history_provinsi"] = $id_prov;
				}

				if(isset($_GET["kotakabupaten"])){
					$id_kota		 = $_GET["kotakabupaten"];
					$this->session->set_userdata('sess_search_kota', $id_kota);
					$data["history_kota"] = $id_kota;
				} else{
					$id_kota = $this->session->userdata('sess_search_kota');
					$data["history_kota"] = $id_kota;
				}

				if(isset($_GET["kecamatan"])){
					$id_kec			 = $_GET["kecamatan"];
					$this->session->set_userdata('sess_search_kec', $id_kec);
					$data["history_kecamatan"] = $id_kec;
				} else{
					$id_kec = $this->session->userdata('sess_search_kec');
					$data["history_kecamatan"] = $id_kec;
				}
				if(isset($_GET["kategori"])){
					$id_kategori		 = $uri_subkategori;
					$this->session->set_userdata('sess_search_kategori', $id_kategori);
					$data["history_kategori"] = $id_kategori;
				} else{
					$id_kategori = $this->session->userdata('sess_search_kategori');
					$data["history_kategori"] = $id_kategori;
				}


			} else{
				$this->session->unset_userdata('history_provinsi');
				$this->session->unset_userdata('history_kota');
				$this->session->unset_userdata('history_kecamatan');
			}



		    $perpage = 6;
		    $data['travel_category_id']     = $uri_kategori;
		    $data['sub_travel_category_id'] = $uri_subkategori;
		    $data['limit']     				= $perpage;
		    $data['offset']     			= $from;
		    $data['province_id']     		= $id_prov;
		    $data['city_id']     			= $id_kota;
		    $data['village_id']     		= $id_kec;
		    $data['travel_name']     		= $this->session->userdata('sess_search_key');		    

			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');

			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$result							= $this->curl->simple_get($this->system['apiUrl'] .'/travel/travels_list', array("jwt" => $jwt));

			$data['result_data'] 			= json_decode($result);


			$data['result_provinsi'] 	= callProvinces();


			if($data['result_data']->status != "ERROR"){
				//JIKA BUKAN DARI MENU MAKA
				if(!is_numeric($namaKategori)){
					$config['base_url']   = site_url("page/kategori/$namaKategori/$uri_kategori/$uri_subkategori/");
				} else{
					$config['base_url']   = site_url("page/kategori/");
				}

				$totalRows =$data['result_data']->results->total;
				

	     		$config['total_rows'] = $totalRows;
	     		$config['per_page']   = $perpage;
	     		
	     		$config['full_tag_open']    = '<ul class="pagination justify-content-end">';
				$config['full_tag_close']   = '</ul>';
				$config['first_link']       = 'First';
				$config['last_link']        = 'Last';
				$config['first_tag_open']   = '<li class="page-item page-link">';
				$config['first_tag_close']  = '</li>';
				$config['prev_link']        = '&laquo';
				$config['prev_tag_open']    = '<li class="page-item page-link">';
				$config['prev_tag_close']   = '</li>';
				$config['next_link']        = '&raquo';
				$config['next_tag_open']    = '<li class="page-item page-link">';
				$config['next_tag_close']   = '</li>';
				$config['last_tag_open']    = '<li class="page-item page-link">';
				$config['last_tag_close']   = '</li>';
				$config['cur_tag_open']     = '<li class="page-item disabled"><a href="" class="page-link">';
				$config['cur_tag_close']    = '</a></li>';
				$config['num_tag_open']     = '<li class="page-item page-link">';
				$config['num_tag_close']    = '</li>';

	     		$this->pagination->initialize($config);
	     		$data['links'] = $this->pagination->create_links();
			}


			if ($data['result_data']->messages == "Authentication Failed") {
				redirect("/logout");
			}else{

				$this->middle = 'frontend/destinasi';
				$this->data = $data;
				$this->layout();
			}

	}

	public function pencarian(){
			$data = array();

			$keyWord    	 = $_GET['keyword'];
			$namaKategori    = $this->uri->segment(3);

			$uri_kategori    = $this->uri->segment(4);
			$uri_subkategori = $this->uri->segment(5);
			$from 			 = $this->uri->segment(6);


			$id_prov = "";
			$id_kota = "";
			$id_kec  = "";
			$data["history_provinsi"] = "";
			$data["history_kota"] = "";
			$data["history_kecamatan"] = "";
			$data["history_traveling_id"] = "";
			$data["history_kategori"] = "";

			if(is_numeric($namaKategori) || empty($namaKategori)){
				$from 			 = $this->uri->segment(3);

				if(isset($_POST["provinsi"])){
					$id_prov		 = $_POST["provinsi"];
					$this->session->set_userdata('sess_search_prov', $id_prov);
					$data["history_provinsi"] = $id_prov;
				} else{
					$id_prov = $this->session->userdata('sess_search_prov');
					$data["history_provinsi"] = $id_prov;
				}

				if(isset($_POST["kotakabupaten"])){
					$id_kota		 = $_POST["kotakabupaten"];
					$this->session->set_userdata('sess_search_kota', $id_kota);
					$data["history_kota"] = $id_kota;
				} else{
					$id_kota = $this->session->userdata('sess_search_kota');
					$data["history_kota"] = $id_kota;
				}

				if(isset($_POST["kecamatan"])){
					$id_kec			 = $_POST["kecamatan"];
					$this->session->set_userdata('sess_search_kec', $id_kec);
					$data["history_kecamatan"] = $id_kec;
				} else{
					$id_kec = $this->session->userdata('sess_search_kec');
					$data["history_kecamatan"] = $id_kec;
				}
				if(isset($_GET["kategori"])){
					$id_kategori		 = $uri_subkategori;
					$this->session->set_userdata('sess_search_kategori', $id_kategori);
					$data["history_kategori"] = $id_kategori;
				} else{
					$id_kategori = $this->session->userdata('sess_search_kategori');
					$data["history_kategori"] = $id_kategori;
				}


			} else{
				$this->session->unset_userdata('history_provinsi');
				$this->session->unset_userdata('history_kota');
				$this->session->unset_userdata('history_kecamatan');
			}



		    $perpage = 6;
		    $data['travel_category_id']     = $uri_kategori;
		    $data['sub_travel_category_id'] = $uri_subkategori;
		    $data['limit']     				= $perpage;
		    $data['offset']     			= $from;
		    $data['province_id']     		= $id_prov;
		    $data['city_id']     			= $id_kota;
		    $data['village_id']     		= $id_kec;
		    $data['travel_name']     		= $keyWord;

			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');

			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$result							= $this->curl->simple_get($this->system['apiUrl'] .'/travel/travels_list', array("jwt" => $jwt));

			$data['result_data'] 			= json_decode($result);


			$data['result_provinsi'] 	= callProvinces();


			if($data['result_data']->status != "ERROR"){
				//JIKA BUKAN DARI MENU MAKA
				if(!is_numeric($namaKategori)){
					$config['base_url']   = site_url("page/kategori/$namaKategori/$uri_kategori/$uri_subkategori/");
				} else{
					$config['base_url']   = site_url("page/kategori/");
				}

				$totalRows =$data['result_data']->results->total;
				

	     		$config['total_rows'] = $totalRows;
	     		$config['per_page']   = $perpage;
	     		
	     		$config['full_tag_open']    = '<ul class="pagination justify-content-end">';
				$config['full_tag_close']   = '</ul>';
				$config['first_link']       = 'First';
				$config['last_link']        = 'Last';
				$config['first_tag_open']   = '<li class="page-item page-link">';
				$config['first_tag_close']  = '</li>';
				$config['prev_link']        = '&laquo';
				$config['prev_tag_open']    = '<li class="page-item page-link">';
				$config['prev_tag_close']   = '</li>';
				$config['next_link']        = '&raquo';
				$config['next_tag_open']    = '<li class="page-item page-link">';
				$config['next_tag_close']   = '</li>';
				$config['last_tag_open']    = '<li class="page-item page-link">';
				$config['last_tag_close']   = '</li>';
				$config['cur_tag_open']     = '<li class="page-item disabled"><a href="" class="page-link">';
				$config['cur_tag_close']    = '</a></li>';
				$config['num_tag_open']     = '<li class="page-item page-link">';
				$config['num_tag_close']    = '</li>';

	     		$this->pagination->initialize($config);
	     		$data['links'] = $this->pagination->create_links();
			}


			if ($data['result_data']->messages == "Authentication Failed") {
				redirect("/logout");
			}else{

				$this->middle = 'frontend/destinasi';
				$this->data = $data;
				$this->layout();
			}
	}

	public function audit(){

			$data = array();

			$from 			 = $this->uri->segment(3);


			$id_prov = 0;
			$id_kota = 0;
			$id_kec  = 0;
			$id_cat  = 0;
			$data["history_point"]	= "";
			$data["history_provinsi"] 	= "";
			$data["history_kota"] 		= "";
			$data["history_kecamatan"] 	= "";

				if(isset($_POST["point"])){
					$id_cat		 = $_POST["point"];
					$this->session->set_userdata('sess_search_point', $id_cat);
					$data["history_point"] = $id_cat;
				} else{
					$id_cat = $this->session->userdata('sess_search_point');
					$data["history_point"] = $id_cat;
				}


				if(isset($_POST["provinsi"])){
					$id_prov		 = $_POST["provinsi"];
					$this->session->set_userdata('sess_search_prov', $id_prov);
					$data["history_provinsi"] = $id_prov;
				} else{
					$id_prov = $this->session->userdata('sess_search_prov');
					$data["history_provinsi"] = $id_prov;
				}

				if(isset($_POST["kotakabupaten"])){
					$id_kota		 = $_POST["kotakabupaten"];
					$this->session->set_userdata('sess_search_kota', $id_kota);
					$data["history_kota"] = $id_kota;
				} else{
					$id_kota = $this->session->userdata('sess_search_kota');
					$data["history_kota"] = $id_kota;
				}

				if(isset($_POST["kecamatan"])){
					$id_kec			 = $_POST["kecamatan"];
					$this->session->set_userdata('sess_search_kec', $id_kec);
					$data["history_kecamatan"] = $id_kec;
				} else{
					$id_kec = $this->session->userdata('sess_search_kec');
					$data["history_kecamatan"] = $id_kec;
				}


		    $perpage = 6;
		    $data['limit']     				= $perpage;
		    $data['offset']     			= $from;
		    $data['point_type_id']     		= $id_cat;
		    $data['province_id']     		= $id_prov;
		    $data['city_id']     			= $id_kota;
		    $data['village_id']     		= $id_kec;
		    $data['travel_id']     			= "";
		    $data['question_id']  			= "";


			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');

			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$result							= $this->curl->simple_get($this->system['apiUrl'] .'/tourism/audit_results', array("jwt" => $jwt));

			$data['result_data'] 			= json_decode($result);

			echo $data['point_type_id']."<br>";
			echo $data['province_id']."<br>";
			echo $data['city_id']."<br>";
			echo $data['village_id']."<br>";
			echo $data['travel_id']."<br>";
			echo $data['question_id']."<br>";

		    print_r($data['result_data']);
			$data['result_provinsi'] 		= callProvinces();
			$data['result_point'] 		    = h_point();


			if ($data['result_data']->messages == "Authentication Failed") {
				redirect("/logout");
			} else{
				exit();
				$this->middle = 'frontend/hasil_audit';
				$this->data = $data;
				$this->layout();
			}

	}

	public function destinasi(){
		//echo 'Hello World';
		$this->middle = 'frontend/destinasi';
		$this->layout();
	}

	public function program(){
		//echo 'Hello World';
		$this->middle = 'frontend/program';
		$this->layout();
	}

	public function potensi(){
		//echo 'Hello World';
		$this->middle = 'frontend/potensi';
		$this->layout();
	}

	public function detailpotensi(){
		//echo 'Hello World';
		$this->middle = 'frontend/detail-potensi';
		$this->layout();
	}

	public function detailprogram(){
		//echo 'Hello World';
		$this->middle = 'frontend/detail-program';
		$this->layout();
	}

	public function detailkunjungan(){
		//echo 'Hello World';
		$this->middle = 'frontend/detail-kunjungan';
		$this->layout();
	}

	public function detailpelaku(){
		//echo 'Hello World';
		$this->middle = 'frontend/detail-pelaku';
		$this->layout();
	}


	public function objek(){

		$data = array();

		$idObjek    = $this->uri->segment(3);


		$data['id'] 					= $idObjek;

		$data['client_id'] 				= $this->system['apiClientId'];
		$data['client_secret'] 			= $this->system['apiClientSecret'];
		$data['token'] 				    = $this->session->userdata('token');
		$data["history_provinsi"] = "";
		$data["history_kota"] = "";
		$data["history_kecamatan"] = "";
		$data["history_traveling_id"] = "";
		$data["history_kategori"] = "";

		$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
		$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'travel/travel_detail', array("jwt" => $jwt));

		$data['result_objek'] 			= json_decode($data['result']);
			
		if ($data['result_objek']->messages == "Authentication Failed") {
				redirect("/logout");
		}else{
			$this->middle = 'frontend/objek-wisata';
			$this->data = $data;
			$this->layout();
		}



		
	}


	public function provinsi($key){
		echo 'Hello World';
		/*$this->middle = 'frontend/destinasi';
		$this->layout();*/
	}

	public function kota($key){
		$this->middle = 'frontend/destinasi';
		$this->layout();
	}
}