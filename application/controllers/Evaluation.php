<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluation extends CI_Controller {
	
	public $template = array();
	public $data = array();

	public function __construct(){

		parent::__construct();

		$this->system = $this->config->item('system');
		$this->load->helper('date');
	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	public function index(){

		if ($this->session->userdata('token') == "") {
			redirect("/");
		}
		else{
			
			$data = array(
				'page_title' 	=> 'Evaluasi',
				'description' 	=> 'Kuisioner Evaluasi Akfis Dustira',
				'title' 			=> 'Evaluasi - Mahasiswa Akfis Dustira',
				'active_evaluation' 		=> 'active_tab'
			);
			
			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');
			$data['tahun']					= mdate("%Y");
			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'/evaluasi', array("jwt" => $jwt));			

			$data['result_data'] 				= json_decode($data['result']);			

			if ($data['result_data']->messages == "Authentication Failed") {
				redirect("/logout");
			}else{
				$this->middle = 'evaluation/index';
				$this->data = $data;
				$this->layout();
			}
		}

	}

	public function create(){		
		$answers = array();
		$no=0;
		foreach($this->input->post('evaluation[]', true) as $key){
		 	$answers[$no] = $this->input->post('pilihan_'.$key, true);		
		 	$no++;
		}		
		$data['client_id'] 		= $this->system['apiClientId'];
		$data['client_secret'] 	= $this->system['apiClientSecret'];
		$data['token'] 			= $this->session->userdata('token');		
		$data['evaluasi'] 		= implode(",",$this->input->post('evaluation[]', true));
		$data['jawaban'] 		= implode(",",$answers);
		$data['tahun'] 			= mdate("%Y");

		$jwt 					= $this->jwt->encode($data, $this->system['jwtKey']);
		$data['result'] 			= $this->curl->simple_post($this->system['apiUrl'] .'/evaluasi/simpan', array("jwt" => $jwt));
		$result_array 			= json_decode($data['result']);

		if ($result_array->messages == 'Success') {
			redirect("/evaluation?notice=".$result_array->results->evaluasi->pesan);
		}else{
			redirect('welcome?notice=API Error');
		}
	}
}
