<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiOpsi extends CI_Controller {
	
	public $template = array();
	public $data = array();

	public function __construct(){

		parent::__construct();

		$this->load->helper('Provinsi');
		$this->load->helper('Kota');
		$this->load->helper('Kecamatan');
		$this->system = $this->config->item('system');
	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/header-portal', $this->template);
	}


	public function callPoint(){
			$data['client_id'] 				= $this->system['apiClientId'];
			$data['client_secret'] 			= $this->system['apiClientSecret'];
			$data['token'] 					= $this->session->userdata('token');

			$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'master/point_types', array("jwt" => $jwt));

			$data['result_point'] 		= json_decode($data['result']);

			
			return $data['result_point'];
	}



	public function kotakabupaten(){
		$id=$this->input->post('id');

		$data['result'] = h_kotakabupaten($id);
			
		echo $data['result'];

	}

	public function kecamatan(){
		$id=$this->input->post('id');

		$data['result'] = h_kecamatan($id);
			
		echo $data['result'];

	}

	public function travels_list(){
		$city_id=$this->input->get('city_id');

		$data['result'] = h_travels($city_id);
			
		echo $data['result'];

	}

	public function subkategori(){
		$id=$this->input->post('id');

		$data = array();

		$this->system = $this->config->item('system');

		$data['client_id'] 				= $this->system['apiClientId'];
		$data['client_secret'] 			= $this->system['apiClientSecret'];
		$data['token'] 					= $this->session->userdata('token');
		$data['travel_category_id'] 	= $id;
			

		$jwt 							= $this->jwt->encode($data, $this->system['jwtKey']);
		$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'master/sub_travel_categories', array("jwt" => $jwt));

			
		echo $data['result'];

	}

}