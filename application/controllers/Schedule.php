<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends CI_Controller {
	
	public $template = array();
	public $data = array();

	public function __construct(){

		parent::__construct();

		$this->system = $this->config->item('system');
	}

	public function layout(){
		$this->template['middle'] = $this->load->view($this->middle, $this->data, true);
		$this->load->view('layouts/application', $this->template);
	}

	public function index()
	{
		if ($this->session->userdata('token') == "") {
			redirect("/");
		}
		else{
			$data = array(
				'page_title'		=> 'Jadwal',
				'description' 	=> 'Informasi Jadwal Mahasiswa',
				'title' 			=> 'Jadwal - Mahasiswa Akfis Dustira',
				'schedule'		=> 'active_tab'
			);

			$data['client_id'] 			= $this->system['apiClientId'];
			$data['client_secret'] 		= $this->system['apiClientSecret'];
			$data['token'] 				= $this->session->userdata('token');			

			if ($this->input->get('schedule') == 'uas') {
				$data['jenis'] 			= $this->input->get('jenis');
				$data['semester_ujian']	= $this->input->get('semester_uas');
				$data['tanggal']		= "";
				$data['semester_tugas']= 0;
				$data['semester'] 		= 0;
				$data['uas']			= "active";
			}else if($this->input->get('schedule') == 'tugas'){
				$data['jenis'] 			= 0;
				$data['semester'] 		= 0;
				$data['semester_ujian']	= 0;
				$data['semester_tugas']= $this->input->get('semester_tugas');
				$data['tugas']			= "active";
				$data['tanggal']		= $this->input->get('tanggal');
			}else{
				$data['jenis'] 			= 0;
				$data['semester'] 		= $this->input->get('semester');
				$data['tanggal']		= "";
				$data['semester_tugas']= 0;
				$data['semester_ujian']	= 0;
				$data['kuliah']			= "active";
			}
			// var_dump($data);
			// exit();
			$jwt 						= $this->jwt->encode($data, $this->system['jwtKey']);

			$data['result'] 				= $this->curl->simple_get($this->system['apiUrl'] .'/jadwal', array("jwt" => $jwt));
			$data['result_data'] 		= json_decode($data['result']);			

			$data['result_uas'] 			= $this->curl->simple_get($this->system['apiUrl'] .'/jadwal/ujian', array("jwt" => $jwt));
			$data['result_data_uas'] 	= json_decode($data['result_uas']);	

			$data['result_tugas'] 		= $this->curl->simple_get($this->system['apiUrl'] .'/jadwal/materi', array("jwt" => $jwt));
			$data['result_data_tugas'] 	= json_decode($data['result_tugas']);			

			if ($data['result_data']->messages == "Authentication Failed" || $data['result_data_uas']->messages == "Authentication Failed" || $data['result_data_tugas']->messages == "Authentication Failed") {
				redirect("/logout");
			}else{
				if(isset($_GET['download'])){
					$this->load->library('r_pdf');
					$html=$this->load->view('schedule/show_pdf',$data, true);
					$pdfFilePath = "Jadwal-".time().".pdf";
					$pdf = $this->r_pdf->load();
					$pdf->WriteHTML($html,2);
					$pdf->Output($pdfFilePath, "D");
				}
				$this->middle = 'schedule/index';
				$this->data = $data;
				$this->layout();
			}
		}
	}
}
