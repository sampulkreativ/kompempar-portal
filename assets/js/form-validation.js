$('form').submit(function(){

	var error = false;

	$(this).find('.data-input').each(function(){

		if ($(this).val() == '' && $(this).attr('data-required') == 'true') {
			error = true;
			// $(this).parent().addClass('has-error');
			$(this).addClass('has-error');
		}

	});

	if (error) {
		$('.form-alert').fadeIn();
		return false;
	}

});

$('.data-input').click(function(){
	// $(this).parent().removeClass('has-error');
	$(this).removeClass('has-error');
});
$(".btn_confirm_password").click(function(){
	if($(".old_password").val() != $(".new_password").val()){
		$("#notice_js").html("<div class='alert alert-danger'>Konfirmasi password tidak sesuai</div><br>");
		return false;
	}
})